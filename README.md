Multi-agent simulation for searching and information retrieval in a decentralized network.

# Configuration

See global.properties for configuration details on data sources, database connections, etc.

# Simulations

To run the master server:

nice -n -19 java -Xmx6096m transeenlight.MainAppLight 0 > transeen.main.log 2>&1 &

To run a slave that connects to the master server:

nice -n -19 java -Xmx4096m transeenlight.MainAppLight 1 localhost 1100 1 > transeen.node1.log 2>&1 &

package transeen.lucene;

import transeen.util.TSDocScore;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.File;
import java.util.Vector;

/**
 * This code was originally written for
 * Erik's Lucene intro java.net article
 */
public class TSLocalSearcher {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      throw new Exception("Usage: java " + TSLocalSearcher.class.getName()
        + " <index dir> <query>");
    }

    String indexDir = args[0];               //1 
    String q = args[1];                      //2   

    search(indexDir, q, 10, 0.5);
  }

  public static Vector<TSDocScore> search(String indexDir, String q, int topDocs, double threshold)
    throws Exception {

    Directory dir = FSDirectory.open(new File(indexDir));
    IndexSearcher is = new IndexSearcher(dir);   //3 
    is.setSimilarity(new NoLengthNormSimilarity()); 

    StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
    QueryParser parser = new QueryParser(Version.LUCENE_CURRENT, "contents", analyzer); //4
    
    Query query = parser.parse(q);              //4   
    long start = System.currentTimeMillis();
    TopDocs hits = is.search(query, topDocs); //5
    long end = System.currentTimeMillis();

    /*System.err.println("Found " + hits.totalHits +   //6  
      " document(s) (in " + (end - start) +
      " milliseconds) that matched query '" +
        q + "':");*/

    Vector<TSDocScore> v = new Vector<TSDocScore>();
    TSDocScore ds;
    for(int i=0;i<hits.scoreDocs.length;i++) {
      ScoreDoc scoreDoc = hits.scoreDocs[i];
      Document doc = is.doc(scoreDoc.doc);  
      String fileName = doc.get("filename");
      fileName = fileName.replaceAll("\\.txt", "");
      ds = new TSDocScore(fileName,scoreDoc.score);
      v.add(ds);
      //System.out.println(doc.get("filename") + "\t" + scoreDoc.score);  //8  
    }
    is.close();
    return v;
  }
}

/*
#1 Index directory created by Indexer
#2 Query string
#3 Open index
#4 Parse query
#5 Search index
#6 Write search stats
#7 Retrieve matching document
#8 Display filename
#9 Close index
*/

package transeen.lucene;

import org.apache.lucene.index.IndexWriter;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import transeen.util.TextReadWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This code was originally written for Erik's Lucene intro java.net article
 */
public class TSLocalIndexer {

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			throw new Exception("Usage: java " + TSLocalIndexer.class.getName()
					+ " <index dir> <data dir>");
		}
		String indexDir = args[0]; // 1
		String dataDir = args[1]; // 2

		buildIndex(indexDir, dataDir);
	}

	/**
	 * build index
	 * 
	 * @param indexDir
	 * @param dataDir
	 * @throws Exception
	 */
	public static void buildIndex(String indexDir, String dataDir) {
		try{
			System.out.println(dataDir + ": indexing starts at " 
					+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			long start = System.currentTimeMillis();
			TSLocalIndexer indexer = new TSLocalIndexer(indexDir);
			int numIndexed = indexer.index(dataDir);
			indexer.close();
			long end = System.currentTimeMillis();
	
			System.out.println(dataDir + "-->" + indexDir + ": indexing " + numIndexed + " files took "
					+ (end - start) + " milliseconds");
		}catch(Exception e){
			System.err.println("Error while indexing " + dataDir + " -> " + indexDir);
			e.printStackTrace();
		}
	}

	private IndexWriter writer;

	public TSLocalIndexer(String indexDir) throws IOException {
		Directory dir = FSDirectory.open(new File(indexDir));
		writer = new IndexWriter(
				dir, // 3
				new StandardAnalyzer(Version.LUCENE_CURRENT), true,
				IndexWriter.MaxFieldLength.UNLIMITED);
		//writer.setMaxBufferedDocs(2000);
		//writer.setRAMBufferSizeMB(100);
	}

	public void close() throws IOException {
		writer.close(); // 4
	}

	public int index(String dataDir) throws Exception {
		/**
		 * if data directory not existent yet, create the directory
		 */
		File myDirFile = new File(dataDir);
		if(myDirFile!=null && !(myDirFile.exists())){
			myDirFile.mkdirs();
		}
	
		/**
		 * if empty data directory, save a couple of text files there to avoid it
		 */
		File[] files = new File(dataDir).listFiles();
		if(files==null || files.length==0){
			TextReadWriter.writeTextFiles(dataDir,2);
		}

		/**
		 * list all text files and index them
		 */
		files = new File(dataDir).listFiles();
		for (File f : files) {
			if (!f.isDirectory() && !f.isHidden() && f.exists() && f.canRead()
					&& acceptFile(f)) {
				indexFile(f);
			}
		}

		return writer.numDocs(); // 5
	}

	protected boolean acceptFile(File f) { // 6
		//return f.getName().endsWith(".txt");
		return true; 
	}

	protected Document getDocument(File f) throws Exception {
		Document doc = new Document();
		doc.add(new Field("contents", readAnchorFile(f),Field.Store.NO, Field.Index.ANALYZED)); // 7
		doc.add(new Field("filename", f.getName(), // 8
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field("fullpath", f.getCanonicalPath(), // 9
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		return doc;
	}

	protected String readAnchorFile(File f){
		String content = "";
		
		try{
			BufferedReader reader = TextReadWriter.readerFile(f.getCanonicalPath());
			String line = reader.readLine();
			
			while(line != null){
				String[] vals = line.split("\t"); 
				if(vals.length>=2){
					double n = new Double(vals[1]).doubleValue();
					n = n + 1.0; 
					n = Math.log(n)/Math.log(2); //n = 7.0 * Math.log(n); 
					for(int i=0;i<n;i++){
						content += " " + vals[0]; 
					}
				}else{
					content += line; 
				}
				line = reader.readLine();
			}
			reader.close();
		}catch(Exception e){
			e.printStackTrace(); 
		}
		
		return content; 
	}
	
	private void indexFile(File f) throws Exception {
		//System.out.println("Indexing " + f.getCanonicalPath());
		Document doc = getDocument(f);
		if (doc != null) {
			writer.addDocument(doc); // 10
		}
	}
	
	
}

/*
 * #1 Create Lucene index in this directory #2 Index *.txt files in this
 * directory #3 Create Lucene IndexWriter #4 Close IndexWriter #5 Return number
 * of documents indexed #6 Index .txt files only #7 Index file content #8 Index
 * file name #9 Index file full path #10 Add document to Lucene index
 */

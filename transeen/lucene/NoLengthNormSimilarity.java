package transeen.lucene;

import org.apache.lucene.search.DefaultSimilarity;

public class NoLengthNormSimilarity extends DefaultSimilarity {

	private static final long serialVersionUID = -3074611143380797834L;

	public static String expSimilarityTF = "exp"; 
	
	public float lengthNorm(String fieldName, int numTerms) {
		return numTerms > 0 ? 1.0f : 0.0f;
	}
	
	/**
	 * normalize tf values
	 */
	public float tf(float freq) {
		float rt = 0; 
		System.out.print(expSimilarityTF.substring(0,1)); 
		if(expSimilarityTF.equalsIgnoreCase("exp"))
			rt = new Double(Math.pow(freq,2.0)).floatValue();
		else if(expSimilarityTF.equalsIgnoreCase("log"))
			rt = new Double(Math.log(freq)).floatValue();
		else
			rt = freq; 
		return rt;
	}

}

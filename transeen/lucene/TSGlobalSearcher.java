package transeen.lucene;

import transeen.database.DatabaseFactory;
import transeen.util.OptionLoader;
import transeen.util.TSDocScore;
import transeen.util.TextReadWriter;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.apache.lucene.wordnet.AnalyzerUtil;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.Iterator;

/**
 * This code was originally written for Erik's Lucene intro java.net article
 */
public class TSGlobalSearcher {

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			throw new Exception("Usage: java "
					+ TSGlobalSearcher.class.getName() + " <index dir> <num docs> <threshold>");
		}

		String indexDir = args[0]; 
		int nDocs = new Integer(args[1]).intValue();
		double threshold = new Double(args[2]).doubleValue();

		searchAllQueries(indexDir, nDocs, threshold);
	}

	public static Vector<TSDocScore> search(String indexDir, String q,
			int topDocs, double threshold) throws Exception {

		Directory dir = FSDirectory.open(new File(indexDir));
		IndexSearcher is = new IndexSearcher(dir); // 3
		is.setSimilarity(new NoLengthNormSimilarity()); 

		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
		QueryParser parser = new QueryParser(Version.LUCENE_CURRENT,
				"contents", analyzer); // 4

		Query query = parser.parse(q); // 4
		long start = System.currentTimeMillis();
		TopDocs hits = is.search(query, topDocs); // 5
		long end = System.currentTimeMillis();

		/*
		 * System.err.println("Found " + hits.totalHits + //6
		 * " document(s) (in " + (end - start) +
		 * " milliseconds) that matched query '" + q + "':");
		 */

		Vector<TSDocScore> v = new Vector<TSDocScore>();
		TSDocScore ds;
		for (int i = 0; i < hits.scoreDocs.length; i++) {
			ScoreDoc scoreDoc = hits.scoreDocs[i];
			Document doc = is.doc(scoreDoc.doc);
			String fileName = doc.get("filename");
			fileName = fileName.replaceAll("\\.txt", "");
			ds = new TSDocScore(fileName, scoreDoc.score);
			ds.setFullPath(doc.get("fullpath"));
			v.add(ds);
			// System.out.println(doc.get("filename") + "\t" + scoreDoc.score);
			// //8
		}
		is.close();
		return v;
	}

	/**
	 * Search all queries on the index
	 */
	public static void searchAllQueries(String indexDir,int nDocs,double threshold) {
		try {
			OptionLoader optionLoader = new OptionLoader(null);
			DatabaseFactory db = new DatabaseFactory();

			db.connect();
			String tbDomains = optionLoader.getOptionString("tbDomains");
			String tbQueries = optionLoader.getOptionString("tbQueries");

			PreparedStatement st = db
					.prepareStatement("select domain,trecid from " + tbQueries);
			ResultSet rs = st.executeQuery();
			int index = 1;
			while (rs.next()) {
				String domain = rs.getString("domain");
				String trecid = rs.getString("trecid");
				String domainPath = optionLoader.getAgentDomainDataPath(domain);
				String fileName = domainPath + "/" + trecid + ".txt";
				String queryString = "";
				if (new File(fileName).exists()) {
					queryString = TextReadWriter.readText(fileName);
					queryString = queryString.replaceAll(
							"[\"\',\\.\\[\\]\\(\\);]", " ");
					int numTerms = optionLoader
							.getOptionInt("maxNumQueryTerms");
					String[] terms = AnalyzerUtil.getMostFrequentTerms(
							new StandardAnalyzer(Version.LUCENE_CURRENT),
							queryString, numTerms);
					queryString = "";
					for (int i = 0; i < terms.length; i++) {
						String term = terms[i].replaceAll("\\d+:", "");
						queryString += " " + term;
					}

					/**
					 * search the index to retrieve relevant documents
					 */
					if (queryString != null && (!queryString.equals(""))) {
						Vector<TSDocScore> myDocs = search(indexDir, queryString, nDocs, threshold);
						Iterator<TSDocScore> it = myDocs.iterator();
						while(it.hasNext()){
							TSDocScore myDoc = it.next();
							System.out.println("RS\t" + index + "\t" + trecid + "\t" + domain + "\t" 
									+ myDoc.getID() + "\t" + myDoc.getScore() + "\t" + myDoc.getFullPath());
							
						}
					}
				}
				index++;
			}
			
			rs.close();
			st.close();
			db.disconnect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

/*
 * #1 Index directory created by Indexer #2 Query string #3 Open index #4 Parse
 * query #5 Search index #6 Write search stats #7 Retrieve matching document #8
 * Display filename #9 Close index
 */

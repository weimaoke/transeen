package transeen.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.index.TermFreqVector;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;

import transeen.util.TSTermFrequency;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public class TSIndexReader {
	protected Directory dir = null;
	protected IndexReader ir = null;
	protected IndexSearcher is = null;

	public TSIndexReader(String indexDir) {
		try {
			dir = FSDirectory.open(new File(indexDir));
			ir = IndexReader.open(dir);
			//is = new IndexSearcher(dir);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if(is != null)
				is.close();
			if(ir != null)
				ir.close();
			// if(dir != null)
			// dir.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * read meta document from an index to describe the collection
	 * 
	 * @param indexDir
	 *            index path
	 * @param minDFVal
	 *            min DF value
	 * @param minDFRate
	 *            min DF rate (in terms of N)
	 * @return meta document text array (first string without frequency; second with term + frequency)
	 * @throws Exception
	 */
	public String[] readMetaDoc(int topTerms, int topQueryTerms, int minDFVal, double minDFRate) {
		String[] docs = new String[2];
		docs[0] = "";
		docs[1] = "";
		try {
			// to select only terms after "a" 
			TermEnum terms = ir.terms(new Term("contents","a"));

			int N = ir.numDocs();
			int minDF = new Double(N * minDFRate).intValue();
			minDF = Math.max(minDF, minDFVal);

			/**
			 * Get terms that meet criterion
			 */
			Vector<TSTermFrequency> v = new Vector<TSTermFrequency>();
			TSTermFrequency tf;
			while (terms.next()) {
				Term term = terms.term();
				String text = term.text();
				int df = ir.docFreq(term);
				if(df >= minDF){
					tf = new TSTermFrequency(text,df);
					v.add(tf);
				}
			}
			// sort terms descendingly in terms of DF
			Comparator<TSTermFrequency> c = Collections.reverseOrder();
			Collections.sort(v, c);
			
			// select top DF terms
			Iterator<TSTermFrequency> it = v.iterator();
			int count = 0;
			TSTermFrequency myTF;
			String myTerm;
			int myFreq = 1;
			int normFreq = 1;
			while (it.hasNext() && count < topTerms) {
				myTF = it.next();
				myTerm = myTF.getTerm();
				myFreq = myTF.getFrequency();
				normFreq = new Double(Math.log(myFreq+1)/Math.log(2)).intValue();
				if(count < topQueryTerms)
					docs[0] += " " + myTerm;
				for(int f=0;f<normFreq;f++){
					docs[1] += " " + myTerm;
				}
				count++;
			}
			/**if(docs[0].equals("")||docs[0].equals(" "))
				docs[0] = "nothing";
			if(docs[1].equals("")||docs[1].equals(" "))
				docs[1] = "nothing";*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return docs;
	}

	/**
	 * retrieve most representative key terms about a document from index
	 * 
	 * @param id
	 *            the document id
	 * @return key terms separated by spaces as a string
	 */
	public String readDocKeyTerms(int id, int numTerms) {
		String terms = "";
		try {
			// Document doc = ir.document(id);
			TermFreqVector tv = ir.getTermFreqVector(id, "contents");
			Vector<TSTermFrequency> v = new Vector<TSTermFrequency>();
			TSTermFrequency tf = null;
			for (int i = 0; i < tv.size(); i++) {
				tf = new TSTermFrequency(tv.getTerms()[i], tv
						.getTermFrequencies()[i]);
				v.add(tf);
			}
			Comparator<TSTermFrequency> c = Collections.reverseOrder();
			Collections.sort(v, c);
			Iterator<TSTermFrequency> it = v.iterator();
			int count = 0;
			while (it.hasNext() && count < numTerms) {
				terms += " " + it.next().getTerm();
				count++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return terms;
	}

	public String readDocKeyTerms(String docID, int numTerms) {
		String file = docID + ".txt";
		Query q = new TermQuery(new Term("filename", file));
		TopDocs hits = null;
		int id = 0;
		try {
			hits = is.search(q, 1);

			if (hits != null) {
				ScoreDoc scoreDoc = hits.scoreDocs[0];
				id = scoreDoc.doc;
				Document doc = is.doc(scoreDoc.doc);
				System.out.println(file + " --> " + id + " --> ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readDocKeyTerms(id, numTerms);
	}

	public static void main(String[] args) throws Exception {
		TSIndexReader wIR = new TSIndexReader(
				"/home/weimao/data/web/_com/_smi/leandactnice_index");
		String[] doc = wIR.readMetaDoc(100, 50, 2, 0.0);
		System.out.println(doc);
	}

}

/*
 * #1 Index directory created by Indexer #2 Query string #3 Open index #4 Parse
 * query #5 Search index #6 Write search stats #7 Retrieve matching document #8
 * Display filename #9 Close index
 */

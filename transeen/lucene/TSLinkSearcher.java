package transeen.lucene;

import transeen.util.TSDocScore;
import transeen.util.TSLinkScore;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.File;
import java.util.Vector;

/**
 * This code was originally written for
 * Erik's Lucene intro java.net article
 */
public class TSLinkSearcher {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      throw new Exception("Usage: java " + TSLinkSearcher.class.getName()
        + " <index dir> <query>");
    }

    String indexDir = args[0];               //1 
    String q = args[1];                      //2   

    search(indexDir, q, 10);
  }

  /**
   * Search link index to determine neighbors' relevance to a query
   * @param indexDir the link index
   * @param q query 
   * @param topn the number of neighbors to considered in the search results
   * @return neighbors relevant (or not so) to the query
   * @throws Exception
   */
  public static Vector<TSLinkScore> search(String indexDir, String q, int topn)
    throws Exception {

    Directory dir = FSDirectory.open(new File(indexDir));
    IndexSearcher is = new IndexSearcher(dir);    
    is.setSimilarity(new NoLengthNormSimilarity()); 
    
    StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
    QueryParser parser = new QueryParser(Version.LUCENE_CURRENT, "contents", analyzer); //4
    
    Query query = parser.parse(q);
    long start = System.currentTimeMillis();
    TopDocs hits = is.search(query, topn);
    long end = System.currentTimeMillis();

    /*System.err.println("Found " + hits.totalHits +   
      " document(s) (in " + (end - start) +
      " milliseconds) that matched query '" +
        q + "':");*/

    Vector<TSLinkScore> v = getLinkHits(is,hits);
    is.close();
    return v;
  }
  
  /**
   * get all links/neighbors from index, regardless of relevance
   * @param indexDir index directory
   * @param topn the number of n links to be returned
   * @return the vectors of links/neighbors
   * @throws Exception
   */
  public static Vector<TSLinkScore> getAllLinks(String indexDir, int topn) throws Exception{
	  Directory dir = FSDirectory.open(new File(indexDir));
	  IndexSearcher is = new IndexSearcher(dir);
	  MatchAllDocsQuery allLinks = new MatchAllDocsQuery();
	  TopDocs hits = is.search(allLinks, topn);
	  Vector<TSLinkScore> v = getLinkHits(is,hits);
	  is.close();
	  return v;
  }
  
  /**
   * Get a vectors of neighbors/links from hits and indexsearcher
   * @param is the index searcher
   * @param hits the hits
   * @return the vectors of hit links 
   * @throws Exception
   */
  public static Vector<TSLinkScore> getLinkHits(IndexSearcher is, TopDocs hits) throws Exception {
	    Vector<TSLinkScore> v = new Vector<TSLinkScore>();
	    TSLinkScore lks;
	    for(int i=0;i<hits.scoreDocs.length;i++) {
	      ScoreDoc scoreDoc = hits.scoreDocs[i];
	      Document doc = is.doc(scoreDoc.doc);  
	      String fileName = doc.get("filename");
	      fileName = fileName.replaceAll("\\.txt", "");
	      
	      int degree = new Integer(doc.get("degree")).intValue();
	      int pages = new Integer(doc.get("pages")).intValue();
	      int pick = new Integer(doc.get("pick")).intValue();      
	      lks = new TSLinkScore(fileName,scoreDoc.score); 
	      lks.setDegree(degree);
	      lks.setPages(pages);
	      lks.setPick(pick);
	      v.add(lks);
	    }
	    return v;
  }
}

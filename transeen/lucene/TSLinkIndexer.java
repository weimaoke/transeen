package transeen.lucene;

import org.apache.lucene.index.IndexWriter;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import transeen.database.DatabaseFactory;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This code was originally written for Erik's Lucene intro java.net article
 */
public class TSLinkIndexer {
	protected String tbDomains = "n2_domains";

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			throw new Exception("Usage: java " + TSLinkIndexer.class.getName()
					+ " <index dir> <data dir>");
		}
		String indexDir = args[0]; // 1
		String dataDir = args[1]; // 2

		buildIndex(indexDir, dataDir, "n2_domains");
	}

	/**
	 * build index
	 * 
	 * @param indexDir
	 * @param dataDir
	 * @throws Exception
	 */
	public static void buildIndex(String indexDir, String dataDir, String domainTable)
			throws Exception {
		System.out.println(dataDir + ": indexing starts at " 
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		long start = System.currentTimeMillis();
		TSLinkIndexer indexer = new TSLinkIndexer(indexDir,domainTable);
		int numIndexed = indexer.index(dataDir);
		indexer.close();
		long end = System.currentTimeMillis();

		System.out.println(dataDir + "-->" + indexDir + ": indexing " + numIndexed + " files took "
				+ (end - start) + " milliseconds");
	}

	private IndexWriter writer;

	public TSLinkIndexer(String indexDir,String domainTable) throws IOException {
		tbDomains = domainTable;
		Directory dir = FSDirectory.open(new File(indexDir));
		writer = new IndexWriter(
				dir, // 3
				new StandardAnalyzer(Version.LUCENE_CURRENT), true,
				IndexWriter.MaxFieldLength.UNLIMITED);
		//writer.setMaxBufferedDocs(2000);
		//writer.setRAMBufferSizeMB(100);
	}

	public void close() throws IOException {
		writer.close(); // 4
	}

	public int index(String dataDir) throws Exception {

		File[] files = new File(dataDir).listFiles();
		DatabaseFactory db = new DatabaseFactory();
		db.connect();		
		
		for (File f : files) {
			if (!f.isDirectory() && !f.isHidden() && f.exists() && f.canRead()
					&& acceptFile(f)) {
				String fileName = f.getName();
				String domain = fileName.replaceAll("\\.txt", "");
				PreparedStatement st = db.prepareStatement("select domain,degree,numpages from " + tbDomains + " where domain=?");
				st.setString(1, domain);
				ResultSet rs = st.executeQuery();
				int degree = 1;
				int numpages = 1;
				int pick = 1;
				if(rs.next()){
					degree = rs.getInt("degree");
					numpages = rs.getInt("numpages");
					pick = 0;
				}
				indexFile(f,degree,numpages,pick);
			}
		}
		db.disconnect();
		return writer.numDocs(); // 5
	}

	protected boolean acceptFile(File f) { // 6
		return f.getName().endsWith(".txt");
	}

	protected Document getDocument(File f) throws Exception {
		Document doc = new Document();
		
		doc.add(new Field("contents", new FileReader(f))); // 7
		doc.add(new Field("filename", f.getName(), // 8
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		// doc.add(new Field("fullpath", f.getCanonicalPath(), // 9
		//		Field.Store.YES, Field.Index.NOT_ANALYZED));
		return doc;
	}

	private void indexFile(File f, int degree, int numpages, int pick) throws Exception {
		//System.out.println("Indexing " + f.getCanonicalPath());
		Document doc = getDocument(f);
		if (doc != null) {
			doc.add(new Field("degree", ""+degree+"", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("pages", ""+numpages+"", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("pick", ""+pick+"", Field.Store.YES, Field.Index.NOT_ANALYZED));
			//doc.add(new NumericField("pick").setIntValue(pick));
			writer.addDocument(doc); // 10
		}
	}
}

/*
 * #1 Create Lucene index in this directory #2 Index *.txt files in this
 * directory #3 Create Lucene IndexWriter #4 Close IndexWriter #5 Return number
 * of documents indexed #6 Index .txt files only #7 Index file content #8 Index
 * file name #9 Index file full path #10 Add document to Lucene index
 */

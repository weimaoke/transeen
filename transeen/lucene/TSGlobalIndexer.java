package transeen.lucene;

import org.apache.lucene.index.IndexWriter;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import transeen.util.TextReadWriter;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This code was originally written for Erik's Lucene intro java.net article
 */
public class TSGlobalIndexer {

	public static void main(String[] args) throws Exception {
		/*String path = "/clueweb/data/_org/_wik/ipedia_";
		System.out.println("Path: " + path);
		if(path.indexOf("_org/_wik/ipedia") >= 0){
			System.out.println("It matches " + path);
		}*/
		
		if (args.length != 2) {
			throw new Exception("Usage: java " + TSGlobalIndexer.class.getName()
					+ " <index dir> <data dir>");
		}
		String indexDir = args[0]; // 1
		String dataDir = args[1]; // 2

		buildIndex(indexDir, dataDir);
		
	}

	/**
	 * build index
	 * 
	 * @param indexDir
	 * @param dataDir
	 * @throws Exception
	 */
	public static void buildIndex(String indexDir, String dataDir) {
		try{
			System.out.println(dataDir + ": indexing starts at " 
					+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			long start = System.currentTimeMillis();
			TSGlobalIndexer indexer = new TSGlobalIndexer(indexDir);
			int numIndexed = indexer.index(dataDir);
			indexer.close();
			long end = System.currentTimeMillis();
	
			System.out.println(dataDir + "-->" + indexDir + ": indexing " + numIndexed + " files took "
					+ (end - start) + " milliseconds");
		}catch(Exception e){
			System.err.println("Error while indexing " + dataDir + " -> " + indexDir);
			e.printStackTrace();
		}
	}

	private IndexWriter writer;
	private long startTime = 0;

	public TSGlobalIndexer(String indexDir) throws IOException {
		Directory dir = FSDirectory.open(new File(indexDir));
		writer = new IndexWriter(
				dir, // 3
				new StandardAnalyzer(Version.LUCENE_CURRENT), true,
				IndexWriter.MaxFieldLength.UNLIMITED);
		//writer.setMaxBufferedDocs(2000);
		//writer.setRAMBufferSizeMB(100);
		startTime = System.currentTimeMillis();
	}

	public void close() throws IOException {
		writer.close(); // 4
	}

	public int index(String dataDir) throws Exception {
		System.out.println("-> to index [" + dataDir + "] at " 
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		File myDirFile = new File(dataDir);
		
		/**
		 * index all directories but wikipedia
		 */
		if(myDirFile.getAbsolutePath().indexOf("_org/_wik/ipedia") < 0){
			File[] files = myDirFile.listFiles();
	
			for (File f : files) {
				/**
				 * index files in the current directory
				 */
				if (!f.isDirectory() && !f.isHidden() && f.exists() && f.canRead()
						&& acceptFile(f)) {
					indexFile(f);
				}
				/**
				 * index files under subdirectories 
				 * recursively
				 */
				if(f.isDirectory()){
					index(f.getAbsolutePath());
				}
			}		
			
			System.out.println("-< indexed [" + dataDir + "] at " 
					+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			files = null;
		}
		myDirFile = null;
		int n = writer.numDocs();
		long nowTime = System.currentTimeMillis();
		System.out.println("Has indexed " + n + " files after " + (nowTime - startTime)/1000 + " seconds.");
		return n;
	}

	protected boolean acceptFile(File f) { // 6
		return f.getName().endsWith(".txt");
	}

	protected Document getDocument(File f) throws Exception {
		Document doc = new Document();
		doc.add(new Field("contents", new FileReader(f))); // 7
		doc.add(new Field("filename", f.getName(), // 8
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field("fullpath", f.getCanonicalPath(), // 9
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		return doc;
	}

	private void indexFile(File f) throws Exception {
		//System.out.println("Indexing " + f.getCanonicalPath());
		Document doc = getDocument(f);
		if (doc != null) {
			writer.addDocument(doc); // 10
		}
	}
}

/*
 * #1 Create Lucene index in this directory #2 Index *.txt files in this
 * directory #3 Create Lucene IndexWriter #4 Close IndexWriter #5 Return number
 * of documents indexed #6 Index .txt files only #7 Index file content #8 Index
 * file name #9 Index file full path #10 Add document to Lucene index
 */

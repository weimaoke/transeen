/**
 * 
 */
package transeen;

import java.io.File;
import java.io.IOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;
import java.util.Hashtable;

import transeen.database.DatabaseFactory;
import transeen.lucene.TSIndexReader;
import transeen.lucene.TSLinkSearcher;
import transeen.lucene.TSLocalIndexer;
import transeen.lucene.TSLocalSearcher;
import transeen.lucene.TSLinkIndexer;
import transeen.util.OptionLoader;
import transeen.util.TSCommon;
import transeen.util.TSLinkScore;
import transeen.util.TSReferenceEntry;
import transeen.util.TSQuery;
import transeen.util.TSDocScore;
import transeen.util.TSTermFrequency;
import transeen.util.TextReadWriter;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

/**
 * A Referral Network of Agents for Expertise/Service Discovery
 * 
 * @author Weimao Ke
 * @author Laboratory of Applied Informatics Research
 * @author University of North Carolina at Chapel Hill
 * @version 0.1
 * 
 */
public class TSAgent extends Agent {

	/**
	 * Generate serial ID
	 */
	private static final long serialVersionUID = 1821959431399944318L;

	/**
	 * agent and neighbor positions
	 */
	protected TSReferenceEntry selfRefEntry = null;
	protected Vector<TSReferenceEntry> neighborRefEntries = null;

	/**
	 * basic agent info
	 */
	protected int agentSID = 1;
	protected int agentDegree = 1;
	protected int agentNumPages = 1;
	protected double currentAlpha = -1.0; // initial value non existence
	protected Hashtable<String, Integer> pickLinks = null;

	DatabaseFactory db = null;

	/**
	 * Various handlers the agent has: an option loader, a data loader, an
	 * information organizer, and a neighbor mapper.
	 */
	protected OptionLoader optionLoader = null;
	protected double expertScore = 0.5;
	protected double localSearchThreshold = 0.5;
	protected int localSearchTopDocs = 10;
	protected int count = 0;
	protected int index = -1;
	protected int maxNumTrainQueries = 500;
	protected int randomTrainQueries = 200;
	protected int learnerOngoingTraining = 0;
	protected int randomExperiment = 0;
	protected int allowSecondBestNeighbor = 5;
	protected int allowRepeatedTimesEach = 2;
	protected int beginRefDegree = 3;
	protected int maxRefLength = 50;

	/**
	 * rewiring variables
	 */
	protected int rewireStart = 1;
	protected int rewireAddRandomNeighbors = 0;
	protected int minD = 1, maxD = 100;
	protected int rewireMinNumNeighbors = 3;
	protected int rewireMaxNumNeighbors = 100;
	protected double rewireHomophilyExponent = 2.0;
	protected double rewireDegreePowerLawExponent = 1;
	protected int saveNeighborDistance = 1;
	protected boolean neighborRewired = false;
	protected int initNumNeighbors = 0;
	protected double rewireProbDegreeChange = 1.0;
	protected double rewirePortionDegreeChange = 0.5;

	/**
	 * variables on status of the current training query
	 */
	protected int startTraining = 0;
	protected int trainingWhenAsked = 1;
	protected boolean hasTrained = false;
	protected boolean queryPending = false;
	protected ACLMessage pendingMessage = null;
	protected String trainQueryID = "";
	protected String trainBestNeighbor = "";
	protected int trainBestNeighborID = 0;
	protected double trainBestScore = 0.0;
	protected double trainSelfScore = 1.0;
	protected int trainNumSent = 0;
	protected int maxRepeatedReferral = 0;
	protected int trainNumReturned = 0;

	/**
	 * TransLucene options
	 */
	protected int luceneForceIndex = 0;
	protected String domainDataPath = null;
	protected String domainIndexPath = null;
	protected boolean domainIndexed = false;

	/**
	 * Initialization of the agent.
	 */
	protected void setup() {
		/**
		 * Initialize the handlers.
		 */
		optionLoader = new OptionLoader(null);
		agentSID = ((Integer) getArguments()[0]).intValue();
		agentDegree = ((Integer) getArguments()[1]).intValue();
		agentNumPages = ((Integer) getArguments()[2]).intValue();
		minD = ((Integer) getArguments()[3]).intValue();
		maxD = ((Integer) getArguments()[4]).intValue();
		db = new DatabaseFactory();

		try {
			currentAlpha = -1.0;
			startTraining = optionLoader.getOptionInt("startTraining");
			trainingWhenAsked = optionLoader.getOptionInt("trainingWhenAsked");
			expertScore = optionLoader.getOptionDouble("classifierExpertScore");
			localSearchThreshold = optionLoader
					.getOptionDouble("localSearchThreshold");
			localSearchTopDocs = optionLoader
					.getOptionInt("localSearchTopDocs");
			maxNumTrainQueries = optionLoader
					.getOptionInt("learnerMaxNumTrainQueries");
			randomTrainQueries = optionLoader
					.getOptionInt("learnerInitRandomTrainQueries");
			learnerOngoingTraining = optionLoader
					.getOptionInt("learnerOngoingTraining");
			randomExperiment = optionLoader.getOptionInt("randomExperiment");
			maxRepeatedReferral = optionLoader
					.getOptionInt("maxRepeatedReferral");
			allowSecondBestNeighbor = optionLoader
					.getOptionInt("allowSecondBestNeighbor");
			beginRefDegree = optionLoader.getOptionInt("beginRefDegree");
			maxRefLength = optionLoader.getOptionInt("maxRefLength");

			/**
			 * rewiring options
			 */
			rewireStart = optionLoader.getOptionInt("rewireStart");
			rewireAddRandomNeighbors = optionLoader
					.getOptionInt("rewireAddRandomNeighbors");
			rewireMinNumNeighbors = optionLoader
					.getOptionInt("rewireMinNumNeighbors");
			rewireMaxNumNeighbors = optionLoader
					.getOptionInt("rewireMaxNumNeighbors");
			rewireHomophilyExponent = optionLoader
					.getOptionDouble("rewireHomophilyExponent");
			rewireDegreePowerLawExponent = optionLoader
					.getOptionDouble("rewireDegreePowerLawExponent");
			saveNeighborDistance = optionLoader
					.getOptionInt("saveNeighborDistance");
			rewireProbDegreeChange = optionLoader
					.getOptionDouble("rewireProbDegreeChange");
			rewirePortionDegreeChange = optionLoader
					.getOptionDouble("rewirePortionDegreeChange");

			/**
			 * TransLucence options
			 */
			luceneForceIndex = optionLoader.getOptionInt("luceneForceIndex");

			domainDataPath = optionLoader
					.getAgentDomainDataPath(getLocalName());
			domainIndexPath = optionLoader
					.getAgentDomainIndexPath(getLocalName());
			domainIndexed = false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println(getLocalName() + " TO SETUP COMMUNICATION.");

		System.out.println(getLocalName() + " READY: d/" + agentDegree + ", p/"
				+ agentNumPages);

		/**
		 * Begin monitoring incoming messages
		 */
		addBehaviour(new MessageMonitor());

		/**
		 * test messaging
		 */
		/**
		 * if(getLocalName().equalsIgnoreCase("smithminerals.com")){
		 * sendMessage("smileandactnice.com",
		 * "HOME smile and act nice: Holiday Giftypoo from HandyGirl"); }
		 */
	}

	/**
	 * Before shutting down the agent.
	 */
	protected void takeDown() {
		/**
		 * Set major object references to nulls.
		 */
		optionLoader = null;

		System.out.println("Agent " + getAID().getName() + " is to take off.");
	}

	/**
	 * Send a query (with a query data instance) to a receiver.
	 * 
	 * @param receiver
	 *            the one who is to receive the query
	 * @param query
	 *            the query instance (RefQuery)
	 */
	protected void sendQuery(String receiver, TSQuery query) {
		ACLMessage msg = new ACLMessage(ACLMessage.QUERY_IF);
		msg.setLanguage("English");
		msg.setOntology("Transeen-Ontology");
		
		try {
			msg.setContentObject(query);
		} catch (IOException e) {
			e.printStackTrace();
		}

		msg.setSender(getAID());
		msg.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		send(msg);
		System.out.print(">> " + receiver + " ");
	}

	/**
	 * get the most close (min distance) agent
	 * 
	 * @param query
	 *            the query (of the target agent)
	 * @return the RefEntry of the closest agent
	 */
	protected TSReferenceEntry getClosestAgent(TSQuery query) {
		TSReferenceEntry bestRefEntry = null;

		return bestRefEntry;
	}

	/**
	 * Generate links to neighbors (extended networking) Links selected here are
	 * not necessarily used in the finalized neighborhood. This provides a large
	 * neighborhood for neighbors to be chosen from.
	 */
	public void generateLinks() {
		String linkPath = optionLoader.getAgentDomainLinkPath(this
				.getLocalName());
		if (!(new File(linkPath)).exists()) {
			try {
				String tbLinks = optionLoader.getOptionString("tbLinks");
				String tbDomains = optionLoader.getOptionString("tbDomains");
				int numAgents = optionLoader.getOptionInt("numAgents");
				int rewireAddRandomNeighbors = optionLoader
						.getOptionInt("rewireAddRandomNeighbors");
				int totalDegree = agentDegree + rewireAddRandomNeighbors;
				int existingDegree = 0;
				db.connect();

				// count existing links
				PreparedStatement st = db
						.prepareStatement("select count(n2) as d from "
								+ tbLinks + " where n1=?");
				st.setInt(1, agentSID);
				ResultSet rs = st.executeQuery();
				if (rs.next()) {
					existingDegree = rs.getInt("d");
				}

				// add random links
				int moreDegree = totalDegree - existingDegree;
				// System.out.println(totalDegree + " - " + existingDegree +
				// " --> " + moreDegree);
				if (moreDegree > 0) {
					st = db.prepareStatement("insert into " + tbLinks
							+ " (n1,n2,pick) values (?,?,?)");
					for (int i = 0; i < moreDegree; i++) {
						int sid2 = TSCommon.getRandom(numAgents) + 1;
						if (sid2 != agentSID) {
							// insert n1 -> n2 record
							st.setInt(1, agentSID);
							st.setInt(2, sid2);
							st.setBoolean(3, false);
							st.executeUpdate();
							// System.out.println("insert " + agentSID + )
							// insert n2 -> n1 record
							st.setInt(2, agentSID);
							st.setInt(1, sid2);
							st.setBoolean(3, false);
							st.executeUpdate();
						}
					}
				}

				// store metadocs (DFs) from neighbors
				boolean success = (new File(linkPath)).mkdirs();
				// if(success){
				String sql = "select distinct t2.domain as domain from "
						+ tbLinks + " as t1 inner join " + tbDomains
						+ " as t2 on t1.n2=t2.sid where t1.n1=?";
				// System.out.println("SID (" + agentSID + "): " + sql);
				st = db.prepareStatement(sql);
				st.setInt(1, agentSID);
				rs = st.executeQuery();
				String domain;
				String domainIndexPath;
				int topTerms = optionLoader.getOptionInt("metadocTopTerms");
				int topQueryTerms = optionLoader
						.getOptionInt("metaQueryTopTerms");
				int minDFVal = optionLoader
						.getOptionInt("metadocTermMinDFValue");
				double minDFRate = optionLoader
						.getOptionDouble("metadocTermMinDFRate");
				while (rs.next()) {
					domain = rs.getString("domain");
					buildDomainIndex(domain);
					linkMetaDocument(getLocalName(), domain, topTerms,
							topQueryTerms, minDFVal, minDFRate);
				}

				// analyze and store self metadocument
				createMetaDocument(getLocalName(), topTerms, topQueryTerms,
						minDFVal, minDFRate);

				// }
				db.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * build data index for a domain
	 * 
	 * @param domain
	 *            the domain to be indexed
	 */
	protected void buildDomainIndex(String domain) {
		String dataPath = optionLoader.getAgentDomainDataPath(domain);
		String indexPath = optionLoader.getAgentDomainIndexPath(domain);
		// build domain index if not done yet
		if (!(new File(indexPath)).exists()) {
			// System.out.println(getLocalName() + ": " + indexPath +
			// " <== " + dataPath);
			TSLocalIndexer.buildIndex(indexPath, dataPath);
		}
	}

	/**
	 * create the meta document for a domain when it does not exist yet with
	 * both thresholds met
	 * 
	 * @param domain
	 *            the domain
	 * @param topTerms
	 *            the top number of terms to be used
	 * @param minDFVal
	 *            min DF threshold
	 * @param minDFRate
	 *            min DF rate threshold
	 */
	protected void createMetaDocument(String domain, int topTerms,
			int topQueryTerms, int minDFVal, double minDFRate) {
		String metaPath = optionLoader.getAgentDomainMetaPath(domain);
		if (!(new File(metaPath)).exists()) {
			(new File(metaPath)).mkdirs();
		}
		String metaPathName = optionLoader.getAgentDomainMetaPathName(domain);
		String metaQueryPathName = optionLoader
				.getAgentDomainMetaQueryPathName(domain);
		try {
			/**
			 * analyze and create the meta doc only when it is not there yet
			 */
			if (!(new File(metaPathName)).exists()) {
				String domainIndexPath = optionLoader
						.getAgentDomainIndexPath(domain);
				TSIndexReader wIR = new TSIndexReader(domainIndexPath);
				String[] metaTexts = wIR.readMetaDoc(topTerms, topQueryTerms,
						minDFVal, minDFRate);
				TextReadWriter.writeText(metaQueryPathName, metaTexts[0]);
				TextReadWriter.writeText(metaPathName, metaTexts[1]);
				wIR.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * create a copy of a meta file as a neighbor/link
	 * 
	 * @param linkDomain
	 *            the current domain that is to build links/neighbors
	 * @param metaDomain
	 *            the neighbor/link that the metadocument is about
	 * @param topTerms
	 *            the top number terms to be used
	 * @param minDFVal
	 *            the min DF threshold
	 * @param minDFRate
	 *            the min DF rate threshold
	 */
	protected void linkMetaDocument(String linkDomain, String metaDomain,
			int topTerms, int topQueryTerms, int minDFVal, double minDFRate) {
		createMetaDocument(metaDomain, topTerms, topQueryTerms, minDFVal,
				minDFRate);
		String metaFile = optionLoader.getAgentDomainMetaPathName(metaDomain);
		String linkFile = optionLoader.getAgentDomainLinkPath(linkDomain) + "/"
				+ metaDomain + ".txt";
		try {
			TextReadWriter.copyText(metaFile, linkFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Index links to neighbors (through their metadocuments) 1) index
	 * metadocuments 2) add field value for "degree" 3) add field value for
	 * "numpages"
	 */
	public void indexLinks() {
		String tbDomains = optionLoader.getOptionString("tbDomains");
		String linkPath = optionLoader.getAgentDomainLinkPath(this
				.getLocalName());
		String linkIndexPath = linkPath + "_index";
		if (!(new File(linkIndexPath)).exists()) {
			try {
				TSLinkIndexer.buildIndex(linkIndexPath, linkPath, tbDomains);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * search documents locally
	 * 
	 * @param query
	 *            the query object (with string)
	 * @return true if success; otherwise, false.
	 */
	public TSDocScore localSearch(TSQuery query) {
		TSDocScore ret = null;
		String queryString = query.getQuery();
		String queryID = query.getID();

		try {
			Vector<TSDocScore> v = TSLocalSearcher.search(optionLoader
					.getAgentDomainIndexPath(getLocalName()), queryString,
					localSearchTopDocs, localSearchThreshold);
			if (v.isEmpty()) {
				ret = null;
			} else {
				ret = v.firstElement();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * search link/neighbor index to determine whom to forward the query to
	 * 
	 * @param query
	 *            the query to be processed
	 * @return the best neighbor (linkscore) to contact
	 */
	public TSLinkScore linkSearch(TSQuery query) {
		TSLinkScore bestLink = null;
		if (query != null) {
			String searchMethod = query.getSearchMethod();
			if (searchMethod.equalsIgnoreCase("RW")) {
				bestLink = linkSearchRW(query);
			} else if (searchMethod.equalsIgnoreCase("SIM")) {
				bestLink = linkSearchSIM(query, "SIM");
			} else if (searchMethod.equalsIgnoreCase("DEG")) {
				bestLink = linkSearchExMethod(query, "DEG");
			} else if (searchMethod.equalsIgnoreCase("SIMDEG")
					|| searchMethod.equalsIgnoreCase("DEGSIM")) {
				bestLink = linkSearchSIM(query, "SIMDEG");
			} else if (searchMethod.equalsIgnoreCase("SIMPGS")
					|| searchMethod.equalsIgnoreCase("PGSSIM")) {
				bestLink = linkSearchSIM(query, "SIMPGS");
			} else if (searchMethod.equalsIgnoreCase("PGS")) {
				bestLink = linkSearchExMethod(query, "PGS");
			}
		}
		return bestLink;
	}

	/**
	 * clean the neighbor/link vector to retain those who have been selected as
	 * real neighbors
	 * 
	 * @param v
	 *            the input vector
	 * @return the cleaned vector
	 */
	protected Vector<TSLinkScore> cleanLinkVector(Vector<TSLinkScore> v) {
		Vector<TSLinkScore> v2 = new Vector<TSLinkScore>();
		Iterator<TSLinkScore> it = v.iterator();
		TSLinkScore link;
		while (it.hasNext()) {
			link = it.next();
			if (pickLinks.containsKey(link.getID())) {
				v2.add(link);
			}
		}
		return v2;
	}

	/**
	 * select a link/neighbor randomly (random walk, RW) ignorant of query
	 * content
	 * 
	 * @param query
	 *            the query with existing chain
	 * @return a random neighbor/link, preferrably not contacted yet
	 */
	protected TSLinkScore linkSearchRW(TSQuery query) {
		TSLinkScore bestLink = null;
		try {
			String linkIndexDir = optionLoader
					.getAgentDomainLinkIndexPath(getLocalName());

			/**
			 * select a random neighbor for multiple times to avoid loop
			 */
			Vector<TSLinkScore> v = TSLinkSearcher.getAllLinks(linkIndexDir,
					rewireMaxNumNeighbors);
			v = cleanLinkVector(v);

			int index = 0;
			int size = v.size();
			for (int i = 0; i < size; i++) {
				index = TSCommon.getRandom(size);
				bestLink = v.get(index);
				if (query == null || !query.hasRefEntry(bestLink.getID())) {
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bestLink;
	}

	/**
	 * similarity score based search method (SIM)
	 * 
	 * @return the chosen link/neighbor
	 */
	protected TSLinkScore linkSearchSIM(TSQuery query, String sMethod) {
		TSLinkScore bestLink = null;
		String queryString = query.getQuery();
		try {
			String linkIndexDir = optionLoader
					.getAgentDomainLinkIndexPath(getLocalName());
			Vector<TSLinkScore> v = TSLinkSearcher.search(linkIndexDir,
					queryString, rewireMaxNumNeighbors);
			v = cleanLinkVector(v);

			/**
			 * reset score according to search method
			 */
			Iterator<TSLinkScore> it = v.iterator();
			if (!sMethod.equalsIgnoreCase("SIM")) {

				while (it.hasNext()) {
					bestLink = it.next();
					if (sMethod.equalsIgnoreCase("SIMDEG")) {
						bestLink.setScore(bestLink.getScore()
								* bestLink.getDegree());
					} else if (sMethod.equalsIgnoreCase("SIMPGS")) {
						bestLink.setScore(bestLink.getScore()
								* bestLink.getPages());
					}
				}
				Comparator<TSLinkScore> c = Collections.reverseOrder();
				Collections.sort(v, c);
			}

			/**
			 * get the best neighbor/link in terms of new scores
			 */
			it = v.iterator();
			while (it.hasNext()) {
				bestLink = it.next();
				/**
				 * TODO: may need to change the logic here for now, not allowing
				 * one agent be involved multiple times
				 */
				if (!query.hasRefEntry(bestLink.getID())) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bestLink;
	}

	/**
	 * search methods based on Degree and Pages
	 * 
	 * @param query
	 *            the query
	 * @return the best link/neighbor
	 */
	protected TSLinkScore linkSearchExMethod(TSQuery query, String sMethod) {
		TSLinkScore bestLink = null;
		try {
			String linkIndexDir = optionLoader
					.getAgentDomainLinkIndexPath(getLocalName());

			/**
			 * list all links/neighbors and set scores according to search
			 * method
			 */
			Vector<TSLinkScore> v = TSLinkSearcher.getAllLinks(linkIndexDir,
					rewireMaxNumNeighbors);
			v = cleanLinkVector(v);
			Iterator<TSLinkScore> it = v.iterator();

			while (it.hasNext()) {
				bestLink = it.next();
				if (sMethod.equalsIgnoreCase("DEG")) {
					// bestLink.setScore(bestLink.getNormDegree(minD,maxD,rewireMinNumNeighbors,rewireMaxNumNeighbors));
					bestLink.setScore(bestLink.getDegree());
				} else if (sMethod.equalsIgnoreCase("PGS")) {
					bestLink.setScore(bestLink.getPages());
				}
			}

			/**
			 * select the best scored neighbor
			 */
			Comparator<TSLinkScore> c = Collections.reverseOrder();
			Collections.sort(v, c);
			it = v.iterator();
			while (it.hasNext()) {
				bestLink = it.next();
				if (!query.hasRefEntry(bestLink.getID())) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bestLink;
	}

	/**
	 * compute connection probabilities based on (topical) distance and decide
	 * what neighbors to keep and what to remove
	 * 
	 * @param numToKeep
	 *            the number of neighbors that should remain
	 * @param homoExp
	 *            the homophily exponent for the probability distribution
	 */
	public void chooseLinks(int numToKeep, double alpha) {
		String metaQueryFile = optionLoader
				.getAgentDomainMetaQueryPathName(getLocalName());
		// System.out.println("Meta path: " + selfMetaPath);
		try {
			String queryString = TextReadWriter.readText(metaQueryFile);
			queryString.replaceAll("  ", " ");
			if (queryString == null || queryString.equals("")
					|| queryString.equals(" ")) {
				queryString = getLocalName();
				queryString = queryString.replaceAll("\\.", " ");
			}
			String linkIndexDir = optionLoader
					.getAgentDomainLinkIndexPath(getLocalName());

			/**
			 * search and load relevant links
			 */
			Vector<TSLinkScore> v = TSLinkSearcher.search(linkIndexDir,
					queryString, rewireMaxNumNeighbors
							+ rewireAddRandomNeighbors);
			Hashtable<String, Double> relLinks = new Hashtable<String, Double>();
			Iterator<TSLinkScore> it = v.iterator();
			TSLinkScore link;
			while (it.hasNext()) {
				link = it.next();
				relLinks.put(link.getID(), link.getScore());
			}
			System.out.print(" rel/" + v.size() + ",");

			/**
			 * retrieve all links that have been indexed
			 */
			Vector<TSLinkScore> vall = TSLinkSearcher.getAllLinks(linkIndexDir,
					rewireMaxNumNeighbors + rewireAddRandomNeighbors);
			System.out.print(" all/" + vall.size() + ",");

			/**
			 * TODO run a couple more loops to accommodate duplicate picks
			 * select numToKeep neighbors in terms of the probability
			 * distribution
			 */
			if (pickLinks != null)
				pickLinks.clear();
			pickLinks = new Hashtable<String, Integer>();
			// TSQuery query = new TSQuery("neighor link picks");
			for (int i = 0; i < numToKeep; i++) {
				double r = Math.random();
				double[] p2 = prepareProbabilities(vall, relLinks, alpha);
				int id = TSCommon.binarySearch(p2, r);

				if (id < vall.size()) {
					link = vall.get(id);
					pickLinks.put(link.getID(), new Integer(1));
					vall.remove(id);
					// query.addRef(new
					// TSReferenceEntry(link.getID(),link.getScore()));
				}
			}

			/**
			 * if there is more to pick, choose random ones
			 */
			// v =
			// TSLinkSearcher.getAllLinks(linkIndexDir,rewireMaxNumNeighbors);
			// for(i=pickLinks.size();i <= numToKeep + 5;i++){
			// index = TSCommon.getRandom(v.size());
			// link = v.get(index);
			// if(link!=null){
			// pickLinks.put(link.getID(), new Integer(1));
			// }
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * prepare a probability array based on 1) all neighbors's
	 * relevance/similarity 2) the alpha value
	 * 
	 * @param vall
	 *            the vector of all neighbors/links
	 * @param relLinks
	 *            the hashtable with name-relevance pairs
	 * @param alpha
	 * @return
	 */
	protected double[] prepareProbabilities(Vector<TSLinkScore> vall,
			Hashtable<String, Double> relLinks, double alpha) {
		double[] p = new double[vall.size()];
		double[] p2 = new double[p.length + 1];
		double sumP = 0.0;

		/**
		 * compute the probability values
		 */
		Iterator<TSLinkScore> it = vall.iterator();
		TSLinkScore link;
		int i = 0;
		while (it.hasNext() && i < rewireMaxNumNeighbors) {
			link = it.next();
			if (relLinks.containsKey(link.getID())) {
				link.setScore(relLinks.get(link.getID()).doubleValue());
			}
			p[i] = Math.pow(link.getDistance(), 0.0 - alpha);
			sumP += p[i];
			i++;
		}
		p2[0] = 0.0;
		for (i = 1; i < p2.length; i++) {
			p2[i] = p2[i - 1] + p[i - 1] / sumP;
		}
		return p2;
	}

	/**
	 * A private class used to continuously check incoming messages.
	 * 
	 * @author Weimao Ke
	 * 
	 */
	private class MessageMonitor extends CyclicBehaviour {

		/**
		 * Generated serial ID
		 */
		private static final long serialVersionUID = -4360794235242310963L;

		/**
		 * The action loop for checking received messages.
		 */
		public void action() {
			ACLMessage msg = myAgent.receive();

			if (msg != null) {
				String ont = msg.getOntology();
				/**
				 * to deal with TSQUERY
				 */
				if (ont != null && ont.equals("Transeen-Ontology")) {
					try {
						TSQuery query;
						query = (TSQuery) msg.getContentObject();
						String queryString = query.getQuery();
						String queryID = query.getID();
						/*
						 * System.out.println(getLocalName() + ", " + queryID +
						 * " from " + msg.getSender().getLocalName());
						 */

						/**
						 * if it is a request query, respond to the request
						 */
						if (query.isRequest()) {
							// build domain index if not done yet
							if (domainIndexed == false
									&& (luceneForceIndex > 0 || !(new File(
											domainIndexPath)).exists())) {
								// System.out.println(getLocalName() + ": " +
								// indexPath +
								// " <== " + dataPath);
								TSLocalIndexer.buildIndex(domainIndexPath,
										domainDataPath);
								domainIndexed = true;
							}

							// search the local document collection
							TSDocScore ds = localSearch(query);

							if (ds != null
									&& (ds.getScore() >= localSearchThreshold || ds
											.getID().equalsIgnoreCase(
													query.getID()))) {
								query.addRef(new TSReferenceEntry(
										getLocalName(), ds.getID(), ds
												.getScore()));
								System.out.print("(" + ds.getID() + ", "
										+ ds.getScore() + ") ");

								// send the result back
								query.setAsResponse();
								query.setStatus(TSQuery.STATUS_SUCCEEDED);
								query.setPrecision(1.0);
								query.setRecall(1.0);
								sendQuery("ALLQUERY", query);
							} else {
								String id = "";
								double score = 0.0;
								if (ds != null) {
									id = ds.getID();
									score = ds.getScore();
								} else {
									id = "NULL";
									score = 0.0;
								}
								query.addRef(new TSReferenceEntry(
										getLocalName(), id, score));
								System.out
										.print("(" + id + ", " + score + ") ");

								if (query.getNumRefEntries() >= query
										.getMaxSearchLength()) {
									query.setAsResponse();
									query.setStatus(TSQuery.STATUS_FAILED);
									query.setPrecision(1.0);
									query.setRecall(0.0);
									sendQuery("ALLQUERY", query);
								} else {
									// generate links to neighbors if not
									// generated
									// yet
									// System.out.println(getLocalName() +
									// ": to generate links...");
									generateLinks();

									// index neighbors if not indexed yet
									// System.out.println(getLocalName() +
									// ": to index links/neighbors...");
									indexLinks();

									/**
									 * TODO: determine (pick) neighbors from the
									 * extended neighborhood to connect (true
									 * neighbors)
									 */
									if (query.getAlpha() != currentAlpha) {
										chooseLinks(agentDegree, query
												.getAlpha());
										System.out.println("[d/" + agentDegree
												+ " -> " + pickLinks.size()
												+ "] ");
										currentAlpha = query.getAlpha();
									}

									// search link/neighborhood index to
									// determine
									// the best neighbor to forward the query
									TSLinkScore link = linkSearch(query);
									if (link != null) {
										/**
										 * if loop (repeated link in the chain),
										 * try a random neighbor
										 */
										if (query.hasRefEntry(link.getID())) {
											// query.setAsResponse();
											// query.setStatus(TSQuery.STATUS_FAILED);
											// sendQuery("ALLQUERY", query);
											link = linkSearchRW(query);
										}

										/*
										 * System.out.println("Query (" +
										 * queryID + "): to forward --> " +
										 * link.getID() + ", s/" +
										 * link.getScore() + ", d/" +
										 * link.getDegree() + ", n/" +
										 * link.getPages() + ", p/" +
										 * link.getPick());
										 */

										query.setAsRequest();
										sendQuery(link.getID(), query);

									} else {
										link = linkSearchRW(query);
										query.setAsRequest();
										sendQuery(link.getID(), query);
									}
								}
							}
						} else {
							/**
							 * if it is a response query, route the response
							 * back
							 */
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println(getLocalName() + " (<< " + msg.getSender().getName() + "): " + msg.toString());
					}
				} else if (ont != null) {	/** non-null non-TSQUERY */
					System.out.println(getLocalName() 
							+ " (<<" + msg.getSender().getName() + "): ["
							+ ont + "], " + msg.toString());
				} else {
					System.out.println(getLocalName() 
							+ " (<<" + msg.getSender().getName() + "): "
							+ msg.toString() + "");
				}
			} else {
				/**
				 * This somehow skips unnecessary loops when there is no
				 * incoming message.
				 */
				block();
			}
		}
	}

}

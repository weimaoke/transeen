package transeen.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TSDatabase {

	/**
	 * get a field value from a specific record of a table
	 * depending on the key value 
	 * @param tableName name of the table
	 * @param fieldName the name of the field that contains the desired information
	 * @param keyName the name of the key field
	 * @param keyVal the value of the key for WHERE condition
	 * @return the value of the desired field as an object
	 */
	public static Object getRecordValue(String tableName, String fieldName,
			String keyName, String keyVal) {
		Object fieldValue = null;
		DatabaseFactory db = new DatabaseFactory();
		try {
			db.connect();
			String sql = "select " + fieldName + " from " + tableName
					+ " where " + keyName + "=?";
			PreparedStatement st = db.prepareStatement(sql);
			st.setString(1, keyVal);
			ResultSet rs = st.executeQuery();
			if(rs.next()){
				fieldValue = rs.getObject(fieldName);
			}
			db.disconnect();
			db.unload();
			db = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fieldValue;
	}

}

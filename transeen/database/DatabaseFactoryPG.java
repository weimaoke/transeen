package transeen.database;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import transeen.util.OptionLoader;

public class DatabaseFactoryPG {

	Connection conn = null;
	protected static OptionLoader optionLoader = null;
	
	public DatabaseFactoryPG(){
		optionLoader = new OptionLoader(null);
	}

	/**
	 * connect to database
	 */
	public void connect() {
		// System.out.println("Checking if Driver is registered with DriverManager.");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException cnfe) {
			System.out.println("Couldn't find the driver!");
			System.out.println("Let's print a stack trace, and exit.");
			cnfe.printStackTrace();
			System.exit(1);
		}

		// System.out.println("Registered the driver ok, so let's make a connection.");

		try {
			// The second and third arguments are the username and password,
			// respectively. They should be whatever is necessary to connect
			// to the database.
			if(optionLoader==null)
				optionLoader = new OptionLoader(null);
			String host = optionLoader.getOptionString("pgHost");
			String port = optionLoader.getOptionString("pgPort");
			String mbase = optionLoader.getOptionString("pgBase");
			String user = optionLoader.getOptionString("pgUser");
			String pass = optionLoader.getOptionString("pgPass");
			String connURL = "jdbc:postgresql://" + host + ":" + port + "/" + mbase;
			// System.out.println(connURL);
			conn = DriverManager.getConnection(connURL, user, pass);
		} catch (SQLException se) {
			System.out
					.println("Couldn't connect: print out a stack trace and exit.");
			se.printStackTrace();
			System.exit(1);
		}

		if (conn != null){
			//System.out.println("Hooray! We connected to the database!");
		}else{
			//System.out.println("We should never get here.");
		}
	}
	
	/**
	 * disconnect from database
	 */
	public void disconnect(){
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		conn = null;
	}
	
	public void unload(){
		if(optionLoader!=null)
			optionLoader = null;
	}
	
	/**
	 * prepare a SQL statement
	 * @param sql SQL to be prepared
	 * @return a prepared statement
	 */
	public PreparedStatement prepareStatement(String sql){
		PreparedStatement st = null;
		if(conn != null){
			try {
				st = conn.prepareStatement(sql);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			st = null;
		}
		return st;
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DatabaseFactoryPG db = new DatabaseFactoryPG();
		db.connect();
		PreparedStatement st = db.prepareStatement("select id from n2_queries");
		try {
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				System.out.println(rs.getInt("id"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

/**
 * 
 */
package transeen;

import java.io.BufferedWriter;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import transeen.database.DatabaseFactory;
import transeen.lucene.TSIndexReader;
import transeen.util.OptionLoader;
import transeen.util.TSCommon;
import transeen.util.TSReferenceEntry;
import transeen.util.TSQuery;
import transeen.util.SampleStatistics;
import transeen.util.TextReadWriter;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;
import org.apache.lucene.wordnet.AnalyzerUtil;

/**
 * A Referral Network of Agents for Expertise/Service Discovery
 * 
 * @author Weimao Ke
 * @author Laboratory of Applied Informatics Research
 * @author University of North Carolina at Chapel Hill
 * @version 0.1
 * 
 */
public class TSQueryAgent extends Agent {

	/**
	 * Generate serial ID
	 */
	private static final long serialVersionUID = 1821959431399944318L;

	/**
	 * logWriter to keep track of the experimental results
	 */
	BufferedWriter logWriter = null;
	BufferedWriter chainWriter = null;
	BufferedWriter surveyWriter = null;
	BufferedWriter doneWriter = null;
	BufferedWriter summaryWriter = null;

	/**
	 * Experimental settings
	 */
	protected double[] expClusteringExponents;
	protected int indClusteringExponent = 0;
	protected String[] expSearchMethods;
	protected int indSearchMethod = 0;
	protected int[] expMaxSearchLengths;
	protected int indMaxSearchLength = 0;

	/**
	 * Various handlers the agent has: an option loader, a data loader, an
	 * information organizer, and a neighbor mapper.
	 */
	protected DatabaseFactory db = null;
	protected OptionLoader optionLoader = null;

	protected int queryIndex = 0;
	protected int maxNumQueries = 5;
	protected int queryNumReturned = 0;
	protected double expertScore = 0.5;

	/**
	 * variables to summarize the final results
	 */
	protected int queryWorking = 0;
	protected int queryFailed = 0;
	protected int querySucceeded = 0;
	SampleStatistics expertScoreStatistics = null;
	SampleStatistics expertsInSampleStatistics = null;
	SampleStatistics bestScoreStatistics = null;
	SampleStatistics degreeStatistics = null;

	/**
	 * variables for summary results
	 */
	// Score\tSearchLength\tSearchTime\tP\tR\tF\nDCG
	protected int rsSuccessCount = 0;
	protected double rsScoreSum = 0.0;
	protected long rsSearchLengthSum = 0;
	protected long rsSearchTimeSum = 0;
	protected double rsPSum = 0.0;
	protected double rsRSum = 0.0;
	protected double rsFSum = 0.0;
	protected double rsNDCGSum = 0.0;
	protected double rsQueryCount = 0.0;

	/**
	 * names of all agents on all nodes
	 */
	protected String[] allAgents;
	/**
	 * names of first degree agents on all nodes
	 */
	protected String[] firstAgents;

	/**
	 * references to queries (query docs)
	 */
	protected String[] allQueryIDs;
	protected String[] allQueryDomains;

	protected double localSearchThreshold = 0.5;
	protected int localSearchTopDocs = 10;

	/**
	 * variables on status of the current training query
	 */
	protected int expertSurvey = 1;
	protected int surveySampleSize = 50;
	protected int surveyExpertNum = 5;
	protected SampleStatistics surveySample = null;
	protected double surveyExpertSTD = 1;

	protected String surveyQueryID = "";
	protected int surveyQueryIndex = 0;
	protected String surveyBestNeighbor = "";
	protected int surveyBestNeighborID = 0;
	protected double surveyBestScore = 0.0;
	protected int surveyNumSent = 0;
	protected int surveyNumReturned = 0;

	/**
	 * Initialization of the agent.
	 */
	protected void setup() {
		/**
		 * Initialize the handlers.
		 */
		optionLoader = new OptionLoader(null);

		try {

			maxNumQueries = optionLoader.getOptionInt("maxNumQueries");
			surveySampleSize = optionLoader.getOptionInt("surveySampleSize");
			surveyExpertNum = optionLoader.getOptionInt("surveyExpertNum");
			expertSurvey = optionLoader.getOptionInt("expertSurvey");
			expertScore = optionLoader.getOptionDouble("classifierExpertScore");
			surveyExpertSTD = optionLoader.getOptionDouble("surveyExpertSTD");

			localSearchThreshold = optionLoader
					.getOptionDouble("localSearchThreshold");
			localSearchTopDocs = optionLoader
					.getOptionInt("localSearchTopDocs");

			/**
			 * initialize the summary table
			 */
			queryWorking = 0;
			queryFailed = 0;
			querySucceeded = 0;
			expertScoreStatistics = new SampleStatistics();
			expertsInSampleStatistics = new SampleStatistics();
			bestScoreStatistics = new SampleStatistics();
			degreeStatistics = new SampleStatistics();

			/**
			 * Load all agent names to an array Load all queries to an array
			 */
			initAgentQueries();

			System.out.println("Hello! QueryAgent " + getAID().getName()
					+ " is ready.");

			/**
			 * print log heading
			 */
			logWriter = TextReadWriter.writerFile(optionLoader
					.getExperimentLogName(allAgents.length)
					+ OptionLoader.LOG_FILE_SUFFIX);
			chainWriter = TextReadWriter.writerFile(optionLoader
					.getExperimentLogName(allAgents.length)
					+ OptionLoader.CHAIN_FILE_SUFFIX);
			TextReadWriter
					.writeLine(
							logWriter,
							"\nAlpha\tMaxSearchLength\tSearchMethod\tID\tQuery\tBestAgent\tBestDoc\tBestScore"
									+ "\tStatus\tStatusName\tSearchLength\tSearchTime\tP\tR\tF\nDCG\n");
			String summaryFile = optionLoader
					.getOverallSummaryFileName(allAgents.length);
			if ((new File(summaryFile)).exists()) {
				summaryWriter = TextReadWriter.writerFile(summaryFile, true);
			} else {
				summaryWriter = TextReadWriter.writerFile(summaryFile);
				TextReadWriter
						.writeLine(
								summaryWriter,
								"TIME\tAlpha\tMaxSearchLength\tSearchMethod\tQueryCount\t"
										+ "Success\tScore\tSearchLength\tSearchTime\tP\tR\tF\tnDCG");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * Begin monitoring incoming messages
		 */
		addBehaviour(new MessageMonitor());

		/**
		 * Begin queries
		 */
		initExpSettings();
		double alpha = expClusteringExponents[indClusteringExponent];
		String searchMethod = expSearchMethods[indSearchMethod];
		int maxL = expMaxSearchLengths[indMaxSearchLength];
		nextQuery(alpha, searchMethod, maxL, localSearchThreshold);
	}

	/**
	 * initialize experiment setting variables
	 */
	protected void initExpSettings() {
		/**
		 * experimental settings
		 */
		expClusteringExponents = optionLoader.getOptionDoubleArray(
				"expClusteringExponents", " ");
		expSearchMethods = optionLoader.getOptionStringArray(
				"expSearchMethods", " ");
		expMaxSearchLengths = optionLoader.getOptionIntArray(
				"expMaxSearchLengths", " ");
		indClusteringExponent = 0;
		indSearchMethod = 0;
		indMaxSearchLength = 0;
	}

	/**
	 * set the next experimental setting indexes
	 * 
	 * @return true if succeeded; false, if all experimental settings have been
	 *         used.
	 */
	protected boolean nextExpSetting() {
		boolean next = true;
		if (indSearchMethod < (expSearchMethods.length - 1)) {
			indSearchMethod++;
		} else {
			indSearchMethod = 0;
			if (indMaxSearchLength < (expMaxSearchLengths.length - 1)) {
				indMaxSearchLength++;
			} else {
				indMaxSearchLength = 0;
				if (indClusteringExponent < (expClusteringExponents.length - 1)) {
					indClusteringExponent++;
				} else {
					next = false;
				}
			}
		}
		System.out.println("\nAlpha: "
				+ expClusteringExponents[indClusteringExponent]
				+ "; Max Search Length: "
				+ expMaxSearchLengths[indMaxSearchLength] + "; Search Method: "
				+ expSearchMethods[indSearchMethod] + ".");
		return next;
	}

	/**
	 * Before shutting down the agent.
	 */
	protected void takeDown() {
		/**
		 * Set major object references to nulls.
		 */
		optionLoader = null;

		try {
			if (logWriter != null) {
				logWriter.close();
				logWriter = null;
			}
			if (chainWriter != null) {
				chainWriter.close();
				chainWriter = null;
			}
			if (surveyWriter != null) {
				surveyWriter.close();
				surveyWriter = null;
			}
			if (summaryWriter != null) {
				summaryWriter.close();
				summaryWriter = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("RefAgent " + getAID().getName()
				+ " is to take off.");
	}

	/**
	 * Initiate agent and query lists
	 */
	protected void initAgentQueries() {
		try {
			db = new DatabaseFactory();
			db.connect();
			String tbDomains = optionLoader.getOptionString("tbDomains");
			String tbQueries = optionLoader.getOptionString("tbQueries");

			// retrieve the list of agents
			int numAgents = optionLoader.getOptionInt("numAgents");
			if (numAgents > 0) {
				allAgents = new String[numAgents];
				firstAgents = allAgents;
			}

			PreparedStatement st = db.prepareStatement("select domain from "
					+ tbDomains);
			ResultSet rs;
			rs = st.executeQuery();
			int index = 0;
			while (rs.next() && index < numAgents) {
				allAgents[index] = rs.getString("domain");
				index++;
			}

			// retrieve the list of queries
			// numQueries = optionLoader.getOptionInt("maxNumQueries");
			if (maxNumQueries > 0) {
				allQueryIDs = new String[maxNumQueries];
				allQueryDomains = new String[maxNumQueries];
			}

			st = db.prepareStatement("select domain,trecid from " + tbQueries);
			rs = st.executeQuery();
			index = 0;
			while (rs.next() && index < maxNumQueries) {
				String domain = rs.getString("domain");
				String trecid = rs.getString("trecid");
				allQueryIDs[index] = trecid;
				allQueryDomains[index] = domain;
				index++;
			}
			maxNumQueries = index;
			rs.close();
			st.close();
			db.disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Send a query (with a query data instance) to a receiver.
	 * 
	 * @param receiver
	 *            the one who is to receive the query
	 * @param query
	 *            the query instance (RefQuery)
	 */
	protected void sendQuery(String receiver, TSQuery query) {
		ACLMessage msg = new ACLMessage(ACLMessage.QUERY_IF);
		msg.setLanguage("English");
		msg.setOntology("Transeen-Ontology");
		
		try {
			msg.setContentObject(query);
		} catch (IOException e) {
			e.printStackTrace();
		}
		msg.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		send(msg);
		System.out.print(">> " + receiver + " ");
	}

	/**
	 * Assign the next query to an agent.
	 */
	public boolean nextQuery(double alpha, String searchMethod, int maxSearchL,
			double newThresholdScore) {
		boolean querySent = false;
		if (queryIndex < allQueryIDs.length) {

			TSQuery newQuery = new TSQuery();
			newQuery.setSid(queryIndex);
			newQuery.setID("Q" + queryIndex);
			newQuery.setThresholdScore(newThresholdScore);
			newQuery.addRef(new TSReferenceEntry(getLocalName(), 0.0));

			// Load query content
			String trecid = allQueryIDs[queryIndex];
			String domain = allQueryDomains[queryIndex];
			System.out.print("\nQuery " + queryIndex + "(" + domain + "/"
					+ trecid + "): ALLQUERY ");
			String domainPath = optionLoader.getAgentDomainDataPath(domain);
			String fileName = domainPath + "/" + trecid + ".txt";
			String queryString = "";
			if (new File(fileName).exists()) {
				try {
					queryString = TextReadWriter.readText(fileName);
					queryString = queryString.replaceAll(
							"[\"\',\\.\\[\\]\\(\\);]", " ");
					int numTerms = optionLoader
							.getOptionInt("maxNumQueryTerms");
					String[] terms = AnalyzerUtil.getMostFrequentTerms(
							new StandardAnalyzer(Version.LUCENE_CURRENT),
							queryString, numTerms);
					queryString = "";
					for (int i = 0; i < terms.length; i++) {
						String term = terms[i].replaceAll("\\d+:", "");
						queryString += " " + term;
					}
					/**
					 * String indexDir =
					 * optionLoader.getAgentDomainIndexPath(domain);
					 * TRECWebIndexReader wIR = new
					 * TRECWebIndexReader(indexDir); int numTerms =
					 * optionLoader.getOptionInt("maxNumQueryTerms");
					 * System.out.println(indexDir + "\t" + trecid + "\t" +
					 * numTerms); queryString = wIR.readDocKeyTerms(trecid,
					 * numTerms);
					 */

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (queryString != null && (!queryString.equals(""))
						&& (!queryString.equals(" "))) {
					newQuery.setID(trecid);
					newQuery.setQuery(queryString);
					newQuery.setAlpha(alpha);
					newQuery.setSearchMethod(searchMethod);
					newQuery.setMaxSearchLength(maxSearchL);
					newQuery.setAsRequest();
					newQuery.setStartSearchTime();
					String name = randomAgent(firstAgents);
					// String name = "lyricsmania.com";
					sendQuery(name, newQuery);
					querySent = true;
				} else {
					querySent = false;
				}
			} else {
				querySent = false;
			}
			queryIndex++;
		}
		return querySent;
	}

	/**
	 * randomly pick an agent as the first agent for querying
	 * 
	 * @param agents
	 *            the list of agents
	 */
	public String randomAgent(String[] agents) {
		String name = null;
		if (agents != null) {
			Random r = new Random();
			int i = r.nextInt(agents.length);
			name = agents[i];
		}
		return name;
	}

	/**
	 * A private class used to continuously check incoming messages.
	 * 
	 * @author Weimao Ke
	 * 
	 */
	private class MessageMonitor extends CyclicBehaviour {

		/**
		 * Generated serial ID
		 */
		private static final long serialVersionUID = -4360794235242310963L;

		/**
		 * The action loop for checking received messages.
		 */
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive();
			if (msg != null) {
				String ont = msg.getOntology();
				/**
				 * to deal with TSQUERY
				 */
				if (ont != null && ont.equals("Transeen-Ontology")) {
					TSQuery query;
					try {
						query = (TSQuery) msg.getContentObject();
						if (query.isResponse()) {
							query.setEndSearchTime();
							try {
								/**
								 * log detailed results
								 */
								// Alpha\tMaxSearchLength\tSearchMethod\tID\tQuery\tBestAgent\tBestDoc\tBestScore\tStatus\tSearchLength\tSearchTime\tP\tR\tF\nDCG
								TextReadWriter.writeLine(logWriter, query
										.getAlpha()
										+ "\t"
										+ query.getMaxSearchLength()
										+ "\t"
										+ query.getSearchMethod()
										+ "\t"
										+ query.getSid()
										+ "\t"
										+ query.getID()
										+ "\t"
										+ query.getBestAgent()
										+ "\t"
										+ query.getBestDoc()
										+ "\t"
										+ query.getBestScore()
										+ "\t"
										+ query.getStatus()
										+ "\t"
										+ query.getStatusString()
										+ "\t"
										+ query.getNumRefEntries()
										+ "\t"
										+ query.getSearchTime()
										+ "\t"
										+ query.getPrecision()
										+ "\t"
										+ query.getRecall()
										+ "\t"
										+ query.getF1()
										+ "\t"
										+ query.getnDCG());

								TextReadWriter
										.writeLine(
												chainWriter,
												"CN\t"
														+ expClusteringExponents[indClusteringExponent]
														+ "\t"
														+ expMaxSearchLengths[indMaxSearchLength]
														+ "\t"
														+ expSearchMethods[indSearchMethod]
														+ "\t"
														+ query
																.getRefEntryNames());
								TextReadWriter
										.writeLine(
												chainWriter,
												"CS\t"
														+ expClusteringExponents[indClusteringExponent]
														+ "\t"
														+ expMaxSearchLengths[indMaxSearchLength]
														+ "\t"
														+ expSearchMethods[indSearchMethod]
														+ "\t"
														+ query
																.getRefEntryScores());

								/**
								 * prepare summary results
								 */
								// TIME\tAlpha\tMaxSearchLength\tSearchMethod\tQueryCount\tSuccess\tScore\tSearchLength\tSearchTime\tP\tR\tF\nDCG\n
								rsQueryCount++;
								if (query.getStatus() == TSQuery.STATUS_SUCCEEDED) {
									rsSuccessCount++;
								}
								rsScoreSum += query.getBestScore();
								rsSearchLengthSum += query.getNumRefEntries();
								rsSearchTimeSum += query.getSearchTime();
								rsPSum += query.getPrecision();
								rsRSum += query.getRecall();
								rsFSum += query.getF1();
								rsNDCGSum += query.getnDCG();

							} catch (Exception e) {
								e.printStackTrace();
							}
							/**
							 * when queries for one experimental setting are
							 * done
							 */
							if (queryIndex >= maxNumQueries) {
								/**
								 * log summary results
								 */
								// TIME\tAlpha\tMaxSearchLength\tSearchMethod\tQueryCount\tSuccess\tScore\tSearchLength\tSearchTime\tP\tR\tF\nDCG\n
								try {
									TextReadWriter
											.writeLine(
													summaryWriter,
													TSCommon.getDateTime()
															+ "\t"
															+ expClusteringExponents[indClusteringExponent]
															+ "\t"
															+ expMaxSearchLengths[indMaxSearchLength]
															+ "\t"
															+ expSearchMethods[indSearchMethod]
															+ "\t"
															+ rsQueryCount
															+ "\t"
															+ rsSuccessCount
																	* 1.0
																	/ rsQueryCount
															+ "\t"
															+ rsScoreSum
																	/ rsQueryCount
															+ "\t"
															+ rsSearchLengthSum
																	* 1.0
																	/ rsQueryCount
															+ "\t"
															+ rsSearchTimeSum
																	* 1.0
																	/ rsQueryCount
															+ "\t"
															+ rsPSum
																	/ rsQueryCount
															+ "\t"
															+ rsRSum
																	/ rsQueryCount
															+ "\t" + rsFSum
															/ rsQueryCount
															+ "\t" + rsNDCGSum
															/ rsQueryCount
															+ "\t");
								} catch (Exception e) {
									e.printStackTrace();
								}

								/**
								 * go to next experimental setting
								 */
								if (nextExpSetting()) {
									queryIndex = 0;
									rsQueryCount = 0;
									rsSuccessCount = 0;
									rsScoreSum = 0;
									rsSearchLengthSum = 0;
									rsSearchTimeSum = 0;
									rsPSum = 0;
									rsRSum = 0;
									rsFSum = 0;
									rsNDCGSum = 0;
								} else {
									System.out
											.println("=============== ALL IS DONE ==============");
								}
							}
							/**
							 * next query for each experimental setting
							 */
							boolean success = false;
							while (queryIndex < maxNumQueries && !success) {
								// System.out.print("Query " + queryIndex);
								double alpha = expClusteringExponents[indClusteringExponent];
								String searchMethod = expSearchMethods[indSearchMethod];
								int maxL = expMaxSearchLengths[indMaxSearchLength];
								success = nextQuery(alpha, searchMethod, maxL,
										localSearchThreshold);
							}
						}
					} catch (UnreadableException e) {
						e.printStackTrace();
						System.err.println(getLocalName() + " (<< " + msg.getSender().getName() + "): " + msg.toString());
					}
					msg = null;
				} else if (ont != null) {
					/** non-null non-TSQUERY */
					System.out.println(getLocalName() + " (<<"
							+ msg.getSender().getName() + "): [" + ont
							+ "], " + msg.toString());
				} else {
					System.out.println(getLocalName() + " (<<"
							+ msg.getSender().getName() + "): "
							+ msg.toString() + "");
				}
			} else {
				/**
				 * This somehow skips unnecessary loops when there is no
				 * incoming message.
				 */
				block();
			}
		}

		/**
		 * Show results back from the agents.
		 * 
		 * @param msg
		 *            the message routed back
		 */
		public void showResults(ACLMessage msg) {
			try {
				System.out.println("MSG\t" + msg.getSender().getLocalName());
				TSQuery query = (TSQuery) msg.getContentObject();
				System.out.println("AQ\t"
						+ query.getID()
						+ "\t"
						+ query.getThresholdScore()
						+ "\t"
						+ surveySample.getNumValuesAbove(query
								.getThresholdScore()) + "\t"
						+ query.getBestAgent() + "\t" + query.getBestScore()
						+ "\t" + query.getRefCount() + "\t"
						+ query.getStatusString());

				// query.printRefEntries();
			} catch (UnreadableException e1) {
				e1.printStackTrace();
			}

		}
	}
}

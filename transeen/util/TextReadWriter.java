package transeen.util;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class TextReadWriter {
	
	/**
	 * To open a URL reader 
	 * @param url the URL to read text from
	 * @return the reader pointer
	 * @throws Exception
	 */
	public static BufferedReader readerURL(String url) throws Exception {
		return new BufferedReader(new InputStreamReader(new URL(url)
				.openStream()));
	}
	
	/**
	 * To open a file reader
	 * @param inputFile the input file name to read text from
	 * @return the reader pointer to the file
	 * @throws Exception
	 */
	public static BufferedReader readerFile(String inputFile) throws Exception {
		return new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
	}
	
	/**
	 * Create a output stream to a file
	 * @param outputFile the file for output
	 * @return the handler/writer to the output file
	 * @throws Exception
	 */
	public static BufferedWriter writerFile(String outputFile) throws Exception {
         return new BufferedWriter(new FileWriter(outputFile));
	}
	
	public static BufferedWriter writerFile(String outputFile,boolean append) throws Exception {
        return new BufferedWriter(new FileWriter(outputFile,append));
	}
	
	/**
	 * Read text from URL and write the text to a file
	 * @param url the URL to read text from
	 * @param outputFile the file to write content to
	 * @throws Exception
	 */
	public static void downloadURL(String url,String outputFile) throws Exception{
		BufferedReader reader = readerURL(url);
		BufferedWriter writer = writerFile(outputFile);
		
		String line = reader.readLine();
		while(line != null){
			writer.write(line);
			writer.newLine();
			line = reader.readLine();
		}
		reader.close();
		writer.close();
	}
	
	/**
	 * Read text from a file and write the text to another
	 * @param fromFile the source text file
	 * @param toFile the target text file
	 * @throws Exception
	 */
	public static void copyText(String fromFile,String toFile) throws Exception{
		BufferedReader reader = readerFile(fromFile);
		BufferedWriter writer = writerFile(toFile);
		
		String line = reader.readLine();
		while(line != null){
			writer.write(line);
			writer.newLine();
			line = reader.readLine();
		}
		reader.close();
		writer.close();
	}
	
	/**
	 * Append a line to an opened output file through BufferedWriter
	 * @param writer the buffered writer (opened)
	 * @param line a line of text
	 * @throws Exception
	 */
	public static void writeLine(BufferedWriter writer,String line) throws Exception{
		if(writer!=null && line != null){
			writer.write(line);
			writer.newLine();
			writer.flush();
		}
	}
		
	/**
	 * Read a list of agents (to be loaded) from a URL/Web page. 
	 * @param url the Web page that contains agent names
	 * @return the list of agent name strings
	 * @throws Exception
	 */
	public static Vector<String> readAgents(String url,String file) throws Exception{
		Vector<String> v = new Vector<String>();
		if(! (new File(file)).exists()){
			downloadURL(url,file);
		}
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		while(line != null){
			//line.replaceAll("\\s.*", "");
			v.add(line);
			line = reader.readLine();
		}
		reader.close();
		return v;
	}
	
	/**
	 * read a list of items from a text file
	 * @param file the file name
	 * @return a vector of all items (each a line)
	 * @throws Exception
	 */
	public static Vector<String> readList(String file) throws Exception{
		Vector<String> v = new Vector<String>();
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		while(line != null){
			v.add(line);
			line = reader.readLine();
		}
		reader.close();
		return v;
	}
	
	public static Hashtable<String,Integer> readHash(String file) throws Exception{
		Hashtable<String,Integer> ht = new Hashtable<String,Integer>();
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		while(line != null){
			ht.put(line,new Integer(1));
			line = reader.readLine();
		}
		reader.close();
		return ht;
	}
	
	public static Hashtable<String,Integer> readHash(String file,long numToKeep) throws Exception{
		Hashtable<String,Integer> ht = new Hashtable<String,Integer>();
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		long i = 1;
		while(line != null && i <= numToKeep){
			ht.put(line,new Integer(1));
			line = reader.readLine();
			i++;
		}
		reader.close();
		return ht;
	}
	
	/**
	 * add more items to a list
	 * @param v the existing list of items
	 * @param file the file containing more items 
	 * @throws Exception
	 */
	public static void readListMore(Vector<String> v,String file) throws Exception{
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		/**
		 * prepare the existing keys
		 */
		Map<String,Integer> keys = new HashMap<String,Integer>();
		Iterator<String> it = v.iterator();
		int index = -1;
		while(it.hasNext()){
			index++;
			String key = it.next();
			Integer val = keys.get(key);
			if(val!=null && val.intValue() > 0){
				keys.put(key, val+1);
				v.remove(index);
			}else{
				keys.put(key, 1);
			}
		}
		
		/**
		 * add the new items if not existed yet
		 */
		while(line != null){
			Integer val = keys.get(line);
			if(val==null){
				v.add(line);
			}			
			line = reader.readLine();
		}
		reader.close();
	}
	
	/**
	 * read a list of items from a text file and return an array of it
	 * @param file the file to read from
	 * @return the array of items
	 * @throws Exception
	 */
	public static String[] readListArray(String file) throws Exception{
		Vector<String> v = readList(file);
		String[] a = new String[v.size()];
		for(int i=0;i<v.size();i++){
			a[i] = v.get(i);
		}
		return a;
	}

	/**
	 * write a list of items to a file
	 * @param file the file to write to
	 * @param vt the vector of the items
	 * @throws Exception
	 */
	public static void writeList(String file,Vector<String> vt) throws Exception{
		BufferedWriter writer = writerFile(file);
		Iterator<String> it = vt.iterator();
		while(it.hasNext()){
			writer.write(it.next());
			writer.newLine();
		}
		writer.flush();
		writer.close();
		writer = null;
	}
	
	public static void writeList(String file,Hashtable<String, Integer> ht) throws Exception{
		BufferedWriter writer = writerFile(file);
		Enumeration<String> en = ht.keys();
		while(en.hasMoreElements()){
			writer.write(en.nextElement());
			writer.newLine();
		}
		writer.flush();
		writer.close();
		writer = null;
	}
	
	/**
	 * write text content to a file
	 * @param file the output file
	 * @param content the text content to be saved
	 * @throws Exception
	 */
	public static void writeText(String file,String content) throws Exception{
		BufferedWriter writer = writerFile(file);
		writer.write(content);
		writer.flush();
		writer.close();
		writer = null;
	}
	
	public static void writeText(String file,String content,boolean append) throws Exception{
		BufferedWriter writer = writerFile(file,append);
		writer.write(content);
		writer.flush();
		writer.close();
		writer = null;
	}
	
	/**
	 * save a couple of text files to avoid empty data folders
	 * @param dataDir
	 * @param numFiles
	 */
	public static void writeTextFiles(String dataDir,int numFiles){
		String noEmpty = "information science retrieval multiple agent system research "
					+ "decentralization scalability scale-free networks information library science"
					+ "scalability findability search engine future complexity visualization "
					+ "collaborative filtering local navigation traversal clustering paradox"
					+ "world wide web peer-to-peer p2p efficiency effectiveness";
		try {
			for(int i=1;i<=numFiles;i++){
				TextReadWriter.writeText(dataDir + "/clueweb-noempty" + i + ".txt", noEmpty);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * read the first line of a file
	 * @param file the file name
	 * @return the first line in string
	 * @throws Exception
	 */
	public static String readFirstLine(String file) throws Exception{
		String ret = null;
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		if(line != null){
			ret = line;
		}
		reader.close();
		return ret;
	}
	
	/**
	 * read the text content of a file
	 * @param file text file name
	 * @return file content
	 * @throws Exception
	 */
	public static String readText(String file) throws Exception{
		String content = "";
		BufferedReader reader = readerFile(file);
		String line = reader.readLine();
		
		while(line != null){
			line.replaceAll("\r", "");
			line.replaceAll("\n", "");
			content += " " + line;
			line = reader.readLine();
		}
		reader.close();
		return content;
	}
	
	public static void main(String[] args) throws Exception {
		BufferedReader reader = readerURL(args[0]);
		String line = reader.readLine();

		while (line != null) {
			System.out.println(line);
			line = reader.readLine();
		}
		reader.close();
	}
	
	/**
	 * remove all files in a directory
	 * @param myDir the directory where files should be removed
	 */
	public static void removeAllFiles(String myDir){
		File directory = new File(myDir);
		File[] files = directory.listFiles();		
		for (File file : files){
			if (!file.delete()){
				System.out.println("Failed to delete files under directory: " + file.getAbsolutePath());
			}
		}
	}
	/**
	 * remove a non-empty directory (and files under it)
	 * @param myDir the directory to be removed
	 */
	public static void removeDirectory(String myDir){
		removeAllFiles(myDir);
		File directory = new File(myDir);
		if(!directory.delete()){
			System.out.println("Failed to delete directory: "+ directory.getAbsolutePath());
		}
	}
}

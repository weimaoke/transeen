package transeen.util;

import java.io.Serializable;
import java.lang.Comparable;

public class TSReferenceEntry implements Comparable<Object>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5679515681533912226L;
	public static final double DISTANCE_MIN = 0.0;
	public static final double DISTANCE_MAX = 1.0;
	
	protected int ID = 0;
	protected String agent = "";
	protected String topDoc = "";
	protected double score = 0.0;
	protected double neighborDistanceSum = 0.0;
	protected int neighborDistanceTimes = 0;
	protected boolean remains = false;
	
	public TSReferenceEntry(String agent){
		setAgent(agent);
		setScore(0.0);
	}
	
	public TSReferenceEntry(String agent,double score){
		setAgent(agent);
		setScore(score);
	}
	public TSReferenceEntry(String agent,String topDoc,double score){
		setAgent(agent);
		setTopDoc(topDoc);
		setScore(score);
	}

	public TSReferenceEntry(int id,String agent,double score){
		setID(id);
		setAgent(agent);
		setScore(score);
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int id) {
		ID = id;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getTopDoc() {
		return topDoc;
	}

	public void setTopDoc(String topDoc) {
		this.topDoc = topDoc;
	}

	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * Use the agent name to represent this entry in string.  
	 * @return the agent name
	 */
	public String toString(){
		return agent; 
	}
	
	/**
	 * Make this comparable in terms of the score. 
	 */
	@Override
	public int compareTo(Object o) {
		// compare two entries in terms of the agent names.
		Double myScore = new Double(this.getScore());
		Double oScore = new Double(((TSReferenceEntry) o).getScore());
		return myScore.compareTo(oScore);
	}

	/**
	 * initialize neighbor distance values
	 * @param sum the sum of all distances
	 * @param times the times of all training values
	 */
	public void initNeighborDistance(double sum,int times){
		neighborDistanceSum = sum;
		neighborDistanceTimes = times; 
	}
	/**
	 * update neighbor distance with a new distance value
	 * @param value a new distance value (from training)
	 */
	public void updateNeighborDistance(double value){
		neighborDistanceSum += value;
		neighborDistanceTimes++;
	}
	/**
	 * get the current value of neighbor distance
	 * if unknown, assume it a very distant neighbor (max distance)
	 * @return the average distance value
	 */
	public double getNeighborDistance(){
		if(neighborDistanceTimes>0){
			return neighborDistanceSum/(neighborDistanceTimes*1.0);
		}else{
			return DISTANCE_MAX;
		}
	}
	/**
	 * present the neighbor distance in a string
	 * @return the string
	 */
	public String getNeighborDistanceString(){
		return getID() + "\t" + getAgent() + "\t" + neighborDistanceSum + "\t" + neighborDistanceTimes + "\t" + getNeighborDistance();
	}
	/**
	 * set the neighbor distance with a string
	 * String format:
	 * ID	Name	DistanceSum	DistanceTimes	Distance
	 */
	public void setNeighborDistanceString(String distString){
		String[] items = distString.split("\t");
		/**
		 * set the string only when the string is properly formatted 
		 * and the name is correct
		 */
		if(items!=null && items.length >= 5){
			if(items[1].equalsIgnoreCase(getAgent())){
				double distance = new Double(items[2]).doubleValue();
				int times = new Integer(items[3]).intValue();
				initNeighborDistance(distance,times);
			}
		}
	}
	/**
	 * retrieve the neighbor name from the neighbor distance string
	 * @param distString
	 * @return
	 */
	public static String getNeighborDistanceName(String distString){
		String ret = null;
		String[] items = distString.split("\t");
		if(items!=null && items.length >= 5){
			ret = items[1];
		}
		return ret;
	}

	public boolean isRemains() {
		return remains;
	}

	public void setRemains(boolean remains) {
		this.remains = remains;
	}
}

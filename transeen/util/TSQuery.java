package transeen.util;

import java.io.Serializable;


import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Iterator;

public class TSQuery implements Serializable {
	
	/** 
	 * Query status constants. 
	 */
	public final static int STATUS_WORKING = 0;
	public final static int STATUS_FAILED = 1;
	public final static int STATUS_SUCCEEDED = 2;
	
	protected boolean request = true;
	protected String currentSender = "";
	protected String currentReceiver = "";
	
	/**
	 * search time variables
	 */
	protected long queryStartTime = 0;
	protected long queryEndTime = 0;
	protected double precision = 0.0;
	protected double recall = 0.0;
	protected double nDCG = 0.0;
	
	/**
	 * experimental settings
	 */
	protected double alpha = 2.0;
	protected int minNeighbors = 50; 
	protected int maxNeighbors = 50; 
	protected String searchMethod = "SIM";
	protected int maxSearchLength = 10;
	protected int moreRelevantSearchLength = 0;
	
	public int count = 0; 

	/**
	 * Generated servial ID
	 */
	private static final long serialVersionUID = -7637942391496303353L; 
	
	/**
	 * The unique identifier of the query. 
	 */
	protected String id = "";
	protected int sid = 0;
	protected String domain = ""; 
	
	/**
	 * The query string
	 */
	protected String query = "";
	
	/** 
	 * The vector storing the referral chain, 
	 * e.g., A1 -> A10 -> A6 -> A2
	 */
	protected Vector<TSReferenceEntry> refEntries = null;
	
	// Create a hash table
	/**
	 * keep track of how many times an agent has been referred to
	 */
    Map<String,Integer> refTimes = null;
	
	protected double bestScore = 0.0;
	protected String bestAgent = "";
	protected String bestDoc = "";
	protected int status = 0;
	protected TSLinkScore lastLinkScore = null;
	
	protected double thresholdScore = 0.0;
	
	/**
	 * the number of repeated referrals in the chain
	 */
	protected int repeatedReferral = 0;
	
	/**
	 * Initialize RefQuery. 
	 */
	public TSQuery(){
		refEntries = new Vector<TSReferenceEntry>();
		refTimes = new HashMap<String,Integer>();
		status = 0;
	}
	
	/**
	 * Initialize RefQuery with a query string. 
	 * @param the query string
	 */
	public TSQuery(String query){
		setQuery(query);
		refEntries = new Vector<TSReferenceEntry>();
		refTimes = new HashMap<String,Integer>();
		status = 0;
	}
	
	/**
	 * Get unique identifier of the query. 
	 * @return the id
	 */
	public String getID() {
		return id;
	}
	/**
	 * Set unique identifier of the query. 
	 * @param id the id to set
	 */
	public void setID(String id) {
		this.id = id;
	}
	/**
	 * Get query string. 
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}
	/**
	 * Set query string. 
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Add a new agent to the reference list--that
	 * the agent has been referred for the query.  
	 * @param agent the agent name to be added to the list
	 */
	public void addRef(TSReferenceEntry agent){
		refEntries.add(agent);
		Integer curValue = refTimes.get(agent.getAgent());
		if(curValue==null){
			refTimes.put(agent.getAgent(), 1);
		}else{
			refTimes.put(agent.getAgent(), curValue+1);
		}
		setBestScore(agent.getAgent(),agent.getTopDoc(),agent.getScore());
		count++;
	}
	
	/**
	 * get the number of times an agent has been referred to in the chain
	 * @param name the name of the agent/neighbor
	 * @return the number of times (occurances) in the referral chain (returns 0 when none)
	 */
	public int getRefTimes(String name){
		Integer val = refTimes.get(name);
		if(val==null){
			return 0;
		}else{
			return val;
		}
	}
	
	/**
	 * Get the referrer that refers to the current agent. 
	 * This is useful when the current agent wants to send the message
	 * back to the one who routes the query here. 
	 * @param currAgent the current agent 
	 * @return the agent proceeding the current agent (WHO refers to me)
	 * @return null if not found or the current agent is the first one
	 */
	public TSReferenceEntry getPrevRef(TSReferenceEntry currAgent){
		TSReferenceEntry prevAgent = null;
		//Iterator<RefEntry> it = refEntries.iterator();
		
		/**
		 * Find the element with a name identical to the current agent
		 * and get the one BEFORE the current one. 
		 * 
		 */
		for(int i=0;i<refEntries.size();i++){
			prevAgent = (TSReferenceEntry) refEntries.elementAt(i);
			if(prevAgent.getAgent().equalsIgnoreCase(currAgent.getAgent())){
				if(i==0){
					prevAgent = null;
				}else{
					prevAgent = refEntries.elementAt(i-1);
				}
				break;
			}
		}
		return prevAgent; 
	}
	
	/**
	 * Get the number of agents (referrers) that have been
	 * involved in the query. 
	 * @return the number of referrers
	 */
	public int getRefCount(){
		return refEntries.size();
	}
	public double getBestScore() {
		return bestScore;
	}

	public String getBestDoc(){
		return bestDoc;
	}
	
	public String getBestAgent() {
		return bestAgent;
	}
	
	/**
	 * Update the best score only when the new score is better. 
	 * @param agent the agent who has the score
	 * @param score the new score
	 */
	public void setBestScore(String agent,String doc,double score){
		if(score>=bestScore){
			bestScore = score;
			bestDoc = doc;
			bestAgent = agent;
		}
	}

	public TSLinkScore getLastLinkScore() {
		return lastLinkScore;
	}

	public void setLastLinkScore(TSLinkScore lastLinkScore) {
		this.lastLinkScore = lastLinkScore;
	}

	public double getThresholdScore() {
		return thresholdScore;
	}

	public void setThresholdScore(double threshold) {
		this.thresholdScore = threshold;
	}

	/**
	 * Get query status. 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * Set query status. 
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getStatusString(){
		String ret = "";
		if(status == 0){
			ret = "WORKING";
		}else if(status == 1){
			ret = "FAILED";
		}else{
			ret = "SUCCEEDED";
		}
		return ret;
	}
	
	/**
	 * To check if the agent has been involved with the query. 
	 * @param name the name of the agent
	 * @return yes or not. 
	 */
	public boolean hasRefEntry(String name){
		return refTimes.containsKey(name);
	}
	
	public boolean hasRefEntryOld(String name){
		TSReferenceEntry refEntry = null;
		boolean ret = false; 
		Iterator<TSReferenceEntry> it = refEntries.iterator();
		while(it.hasNext()){
			refEntry = it.next();
			if(refEntry.getAgent().equalsIgnoreCase(name)){
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	/**
	 * Get the number of agents that have been involved in this query. 
	 * @return the number of agents that have been involved in this query.
	 */
	public int getNumRefEntries(){
		return refEntries.size();
	}
	
	/**
	 * Print the query content. 
	 * @return the query content. 
	 */
	public String toString(){
		return this.getQuery();
	}
	
	/**
	 * get a refentry (agent) given the index
	 * @param index the index in the vector
	 * @return the refentry of the agent
	 */
	public TSReferenceEntry getRefEntry(int index){
		TSReferenceEntry refEntry = null;
		if(index >=0 && index < refEntries.size()){
			refEntry =refEntries.get(index);
		}else{
			refEntry = new TSReferenceEntry("NOTHING",0.0);
		}
		return refEntry;
	}
	
	public void printRefEntries(){
		Iterator<TSReferenceEntry> it = refEntries.iterator();
		System.out.print("RF:\t");
		while(it.hasNext()){
			System.out.print(it.next().getAgent() + " -> ");
		}
		System.out.print("\n");
	}
	
	/**
	 * get the reference chain with names in string
	 * @return the string of the chain
	 */
	public String getRefEntryNames(){
		Iterator<TSReferenceEntry> it = refEntries.iterator();
		String chain = "";
		while(it.hasNext()){
			chain += it.next().getAgent() + ">";
		}
		return chain;
	}
	
	/**
	 * get the reference chain with scores in string
	 * @return the string of the scores
	 */
	public String getRefEntryScores(){
		Iterator<TSReferenceEntry> it = refEntries.iterator();
		String chain = "";
		while(it.hasNext()){
			chain += it.next().getScore() + ">";
		}
		return chain;
	}
	
	/**
	 * check the status using the status code and the best score achieved
	 * @return
	 */
	public boolean hasSucceeded(){
		boolean ret = false;
		if(status == TSQuery.STATUS_SUCCEEDED){
			ret = true;
		}else if(bestScore >= thresholdScore){
			ret = true;
			status = TSQuery.STATUS_SUCCEEDED;
		}
		return ret;
	}
	
	/**
	 * add to the number of repeated referrals
	 * @param num the number to add
	 */
	public void addRepeatedReferral(int num){
		repeatedReferral += num;
	}
	/**
	 * add 1 (default) to the number of repeated referrals
	 */
	public void addRepeatedReferral(){
		addRepeatedReferral(1);
	}
	
	/**
	 * to check the repeated referrals have reached the limit
	 * @param max the maximum to be reached
	 * @return true or false (reached limit or not)
	 */
	public boolean reachedRepeatedMax(int max){
		if(repeatedReferral >= max){
			return true;
		}else{
			return false;
		}
	}
	
	public void setAsRequest(){
		request = true;
	}
	public void setAsResponse(){
		request = false;
	}
	public boolean isRequest(){
		return request;
	}
	public boolean isResponse(){
		return !request;
	}
	
	public void setStartSearchTime(){
		queryStartTime = System.currentTimeMillis();
	}
	public void setEndSearchTime(){
		queryEndTime = System.currentTimeMillis();
	}
	public long getSearchTime(){
		return queryEndTime - queryStartTime; 
	}
	
	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public int getMinNeighbors() {
		return minNeighbors;
	}

	public void setMinNeighbors(int minNeighbors) {
		this.minNeighbors = minNeighbors;
	}

	public int getMaxNeighbors() {
		return maxNeighbors;
	}

	public void setMaxNeighbors(int maxNeighbors) {
		this.maxNeighbors = maxNeighbors;
	}

	public String getSearchMethod() {
		return searchMethod;
	}

	public void setSearchMethod(String searchMethod) {
		this.searchMethod = searchMethod;
	}

	public int getMaxSearchLength() {
		return maxSearchLength;
	}

	public void setMaxSearchLength(int maxSearchLength) {
		this.maxSearchLength = maxSearchLength;
	}

	public int getMoreRelevantSearchLength() {
		return moreRelevantSearchLength;
	}

	public void setMoreRelevantSearchLength(int moreRelevantSearchLength) {
		this.moreRelevantSearchLength = moreRelevantSearchLength;
	}
	
	public void reduceMoreRelevantSearchLength(){
		this.moreRelevantSearchLength--;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public double getnDCG() {
		return nDCG;
	}

	public void setnDCG(double nDCG) {
		this.nDCG = nDCG;
	}
	
	public double getF1(){
		double f1 = 0.0;
		if((precision + recall) > 0.0)
			f1 = 2.0*precision*recall/(precision + recall);
		return f1;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getCurrentSender() {
		return currentSender;
	}

	public void setCurrentSender(String currentSender) {
		this.currentSender = currentSender;
	}

	public String getCurrentReceiver() {
		return currentReceiver;
	}

	public void setCurrentReceiver(String currentReceiver) {
		this.currentReceiver = currentReceiver;
	}
	
	/**
	 * get the best agents for the query 
	 * @param topN the number of top agents
	 * @return the array of best agents (in terms of their scores)
	 */
	public TSReferenceEntry[] getBestAgents(int topN){
		TSReferenceEntry[] agents = new TSReferenceEntry[topN];
		Vector<TSReferenceEntry> v = refEntries;
		Comparator<TSReferenceEntry> c = Collections.reverseOrder();
		Collections.sort(v, c);
		Iterator<TSReferenceEntry> it = v.iterator();
		for(int i=0;i<topN;i++){
			if(it.hasNext()){
				agents[i] = it.next();
			}else{
				agents[i] = null;
			}
		}
		return agents;
	}
}

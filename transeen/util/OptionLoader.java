package transeen.util;

import java.util.Properties;
//import java.util.Date;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;

/**
 * A Referral Network of Agents for Expertise/Service Discovery 
 * 
 * This module is used for loading options, i.e., default option values
 * and/or the agent's specific options if available. 
 * 
 * @author Weimao Ke
 * @author Laboratory of Applied Informatics Research
 * @author University of North Carolina at Chapel Hill
 * @version 0.1
 *
 */
public class OptionLoader {
	/**
	 * The default file for options. 
	 */
	public final static String OPTION_FILE_GLOBAL = "global";
	public final static String OPTION_FILE_SUFFIX = ".properties";
	
	/**
	 * Log files
	 */
	public final static String LOG_FILE_SUFFIX = ".log";
	public final static String CHAIN_FILE_SUFFIX = ".chain";
	public final static String SURVEY_FILE_SUFFIX = ".survey";
	public final static String TRAIN_FILE_SUFFIX = ".train";
	
	/**
	 * OVERALL OUTPUT FILE
	 */
	public final static String OVERALL_SUMMARY_FILE = "SUMMARY";
	public final static String OVERALL_DONE_FILE = "DONE";
	
	protected String optionFile = null;
	protected String logFileName = null;
	
	/**
	 * The Properties objects to be loaded
	 */
	protected Properties globalOptions = null;
	protected Properties agentOptions = null;
	
	public OptionLoader(String agentName){
		setOptionFile(agentName); 
		globalOptions = PropertyLoader.loadProperties(OPTION_FILE_GLOBAL,null); 
		agentOptions = PropertyLoader.loadProperties(getOptionFile(),null); 
	}
	
	/**
	 * get the global option file name 
	 * @return the file name with extension
	 */
	public String getGlobalFileName(){
		return getOptionString("appDir") + "/" + OPTION_FILE_GLOBAL + OPTION_FILE_SUFFIX;
	}

	/**
	 * Get the option file name (without .properties)
	 * @return the optionFile
	 */
	public String getOptionFile() {
		return optionFile;
	}

	/**
	 * Set option file name (without .properties)
	 * @param optionFile the optionFile to set
	 */
	public void setOptionFile(String optionFile) {
		if(optionFile != null){
			this.optionFile = optionFile;
		}else{
			this.optionFile = OPTION_FILE_GLOBAL;
		}
	}
	
	/**
	 * Get option value in STRING given a key. 
	 * @param key the key or identification string
	 * @return the value of the key in STRING
	 */
	public String getOptionString(String key){
		String value = null;
		/**
		 * Get value for the key in the global configuration first. 
		 */
		if(globalOptions.containsKey(key)){
			value = globalOptions.getProperty(key);
		}
		/**
		 * Override the global option value if the agent
		 * has a particular value for the key also. 
		 */
		if(agentOptions.containsKey(key)){
			value = agentOptions.getProperty(key);
		}
		return value;
		// nIterations = new Integer(myOptions.getProperty("NIterations")).intValue() ;	
		
	}
	
	/**
	 * Get option value in INT given a key. 
	 * @param key the key or identification string
	 * @return the value in INT
	 */
	public int getOptionInt(String key){
		return new Integer(getOptionString(key)).intValue();
	}
	
	/**
	 * Get option value in DOUBLE given a key. 
	 * @param key the key or identification string
	 * @return the value in DOUBLE
	 */
	public double getOptionDouble(String key){
		return new Double(getOptionString(key)).doubleValue();
	}
	
	/**
	 * Get option values in a string array given a key. 
	 * @param key the key or identification string. 
	 * @param reg the delimiting regular expression
	 * @return the values of the key in a string array. 
	 */
	public String[] getOptionStringArray(String key,String reg){
		String optionString = getOptionString(key);
		String[] stringArray = null;
		if(optionString != null){
			optionString = optionString.trim();
			if(!optionString.equals("")){
				// optionString.split(reg);
				try {
					//stringArray = weka.core.Utils.splitOptions(optionString);
					stringArray = optionString.split(" ");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return stringArray;
	}
	
	/**
	 * Get option values in an INT array given a key. 
	 * @param key the key or identification string. 
	 * @param reg the delimiting regular expression
	 * @return the values of the key in an INT array. 
	 */
	public int[] getOptionIntArray(String key,String reg){
		String[] stringArray = getOptionStringArray(key,reg);
		int[] intArray = new int[stringArray.length];
		for(int i=0;i<intArray.length;i++){
			intArray[i] = new Integer(stringArray[i]).intValue();
		}
		return intArray;
	}
	
	/**
	 * Get option values in an INT array given a key. 
	 * @param key the key or identification string. 
	 * @param reg the delimiting regular expression
	 * @return the values of the key in an INT array. 
	 */
	public double[] getOptionDoubleArray(String key,String reg){
		String[] stringArray = getOptionStringArray(key,reg);
		double[] doubleArray = new double[stringArray.length];
		for(int i=0;i<doubleArray.length;i++){
			doubleArray[i] = new Double(stringArray[i]).doubleValue();
		}
		return doubleArray;
	}
	
	/**
	 * Get the URL that has the list of agents for the node
	 * @param node the JADE node number
	 * @return the URL
	 */
	public String getAgentListURL(String node){
		return getOptionString("agentListURL")+"?node=" + node 
					+ "&degree=" + getOptionString("networkDegree")
					+ "&numPerNode=" + getOptionString("numAgentsPerNode");
	}
	/**
	 * Get the file name to the copy of agent list for the given node
	 * @param node the JADE node number
	 * @return the file name
	 */
	public String getAgentListFile(String node){
		return getOptionString("dataDir")+"/"+ getOptionString("nodeDataPrefix") + "-" + node + "-agents.txt";
	}
	
	/**
	 * The agents within the first degree that will be assigned the queries
	 * @return the URL or file of such a list
	 */
	public String getFirstAgentListURL(){
		return getOptionString("agentListURL")+"?node=ALL&degree=1";
	}
	public String getFirstAgentListFile(){
		return getOptionString("dataDir")+"/"+ getOptionString("nodeDataPrefix") + "-ALL-FirstAgents.txt";
	}
	
	/**
	 * get the file name (without extension) for logging
	 * @param numAgents the number of agents to experiment on
	 * @return the file name (without extension)
	 */
	public String getExperimentLogName(int numAgents){
		if(logFileName == null){
			logFileName = getOptionString("logPrefix") 
					+ "-n" + numAgents 
					+ "-" + TSCommon.getDateTime();
		}
		System.out.println("LOG FILE: " + getOptionString("logDir") + "/" + logFileName); 
		return getOptionString("logDir") + "/" + logFileName;
	}
    
    /**
     * get the file name that contains overall experimental results
     * @param numAgents the number of agents to experiment on
     * @return the file name
     */
    public String getOverallSummaryFileName(int numAgents){
    	return getOptionString("logDir") + "/" + OptionLoader.OVERALL_SUMMARY_FILE + "-N" + numAgents + ".txt";
    }
	
    /**
     * get the file name that has the "done" flag when the experiment is finished
     * @return the done file name
     */
    public String getOverallDoneFileName(){
    	return getOptionString("logDir") + "/" + OptionLoader.OVERALL_DONE_FILE;
    }
    
	/**
	 * Get the URL that has the agent's documents (in .ARFF format)
	 * @param name the name of the agent
	 * @return the URL to the agent's data collection
	 */
	public String getAgentDocsURL(String name){
		return getOptionString("agentDocsURL")+"?name=" + name + "&degree=" + getOptionString("networkDegree");
	}
	/**
	 * Get the file name of the copy of the agent's doc collection
	 * @param name the name of the agent
	 * @return the file name
	 */
	public String getAgentDocsFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("agentDataPrefix") + "-" + name + ".arff";
	}
	
	public String getAgentTXTFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("agentDataPrefix") + "-" + name + ".txt";
	}
	
	/**
	 * Get the file name of the copy of the agent's doc collection in term VECTORS
	 * @param name the name of the agent
	 * @return the file name of the vectorized collection
	 */
	public String getAgentDocsVFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("vectorDataPrefix") + "-" + name + ".arff";
	}	
	/**
	 * Get the file name of the copy of the agent's doc collection in term VECTORS
	 * after clustering and (cluster) labels assigned. 
	 * @param name the name of the agent
	 * @return the file name of the vectorized and labeled collection
	 */
	public String getAgentDocsVLabeledFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("clusterDataPrefix") + "-" + name + ".arff";
	}
	
	public String getAgentDataAttributesFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("attributesDataPrefix") + "-" + name + ".arff";
	}	
	
	public String getAgentClassifierFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("classifierModelPrefix") + "-" + name + ".model";
	}
	
	public String getAgentNeighborListURL(String name){
		return getOptionString("agentNeighborListURL") + "?name=" + name + "&degree=" + getOptionString("networkDegree");
	}
	
	public String getAgentNeighborListFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborListPrefix") + "-" + name + ".txt";
	}
	
	public String getAgentNeighborMoreListFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborMoreListPrefix") + "-" + name + ".txt";
	}	
	
	public String getAgentNeighborRewiredListFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborRewiredListPrefix") + "-" + name + ".txt";
	}
	
	public String getAgentNeighborDistanceFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborDistancePrefix") + "-" + name + ".txt";
	}
	
	public String getAgentNeighborTrainSetFile(String name,boolean rewired){
		if(rewired){
			return getOptionString("dataDir")+"/" + getOptionString("neighborTrainSetPrefix") + "Rewired-" + name + ".arff";
		}else{
			return getOptionString("dataDir")+"/" + getOptionString("neighborTrainSetPrefix") + "-" + name + ".arff";
		}
	}
	public String getAgentNeighborTrainSetFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborTrainSetPrefix") + "-" + name + ".arff";
	}
	
	public String getAgentNeighborLearnerFile(String name,boolean rewired){
		if(rewired){
			return getOptionString("dataDir")+"/" + getOptionString("neighborLearnerPrefix") + "Rewired-" + name + ".model";
		}else{
			return getOptionString("dataDir")+"/" + getOptionString("neighborLearnerPrefix") + "-" + name + ".model";
		}
	}	
	public String getAgentNeighborLearnerFile(String name){
		return getOptionString("dataDir")+"/" + getOptionString("neighborLearnerPrefix") + "-" + name + ".model";
	}
	
	/**
	 * Get path to domain data (e.g., for trec web track)
	 * @param domain domain name
	 * @return data path
	 */
	public String getAgentDomainDataPath(String domain){
		return getAgentDomainPath("dataDir",domain);
	}
	
	public String getAgentDomainPath(String pathName,String domain){
		String path = getOptionString(pathName);
		domain = domain.toLowerCase();
		String[] dds = domain.split("\\.");
		int size = dds.length;		
		
		// only get 3 levels at most
		if(size > 3){
			size = 3;
		}
		for(int i=1;i<=size;i++){
			String part = dds[size-i];
			if(part.length() <= 3){
				path = path + "/_" + part;
			}else{
				String part1 = part.substring(0, 3);
				String part2 = part.substring(3);
				path = path + "/_" + part1 + "/" + part2;
			}
		}
		return path + "_";
	}
	
	/**
	 * get the meta file name of the domain (for neighbor link)
	 * @param domain the domain
	 * @return the meta file name (with full path)
	 */
	public String getAgentDomainMetaPathName(String domain){
		String selfMetaPath = getAgentDomainPath("linkDir", domain);
		return selfMetaPath + "/meta.txt";
	}
	public String getAgentDomainMetaQueryPathName(String domain){
		String selfMetaPath = getAgentDomainPath("linkDir", domain);
		return selfMetaPath + "/metaquery.txt";
	}
	public String getAgentDomainMetaPath(String domain){
		String selfMetaPath = getAgentDomainPath("linkDir", domain);
		return selfMetaPath;
	}
	
	public String getAgentDomainPickLinkFileName(String domain, double alpha, int minN, int maxN){
		String selfMetaPath = getAgentDomainPath("linkDir", domain);
		String tbLinks = getOptionString("tbLinks");
		return selfMetaPath + "/" + tbLinks + "-pick-" + minN + "-" + maxN + "_" + alpha + ".txt";
	}
	
	public String getAgentDomainIndexPath(String domain){
		return getAgentDomainPath("indexDir",domain);
	}
	
	public String getAgentDomainLinkPath(String domain){
		String path = getAgentDomainPath("linkDir",domain);
		String tbLinks = getOptionString("tbLinks");
		return path + "/" + tbLinks;
	}
	
	public String getAgentDomainLinkIndexPath(String domain){
		return getAgentDomainLinkPath(domain) + "_index";
	}
}

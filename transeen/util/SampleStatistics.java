package transeen.util;

import java.util.Collections;
import java.util.Vector;
import java.util.Iterator;

/**
 * A java class for calculating statistics values of a sample
 * @author Weimao Ke
 * @author Laboratory of Applied Informatics Research
 * @author University of North Carolina at Chapel Hill
 * @version 0.1
 */
public class SampleStatistics {
	protected Vector<Double> data = null;
	protected double min = 0;
	protected double max = 0;
	protected double sum = 0;
	
	public SampleStatistics(){
		data = new Vector<Double>();
	}
	
	/**
	 * add a new value to the data and update relevant statistics
	 * @param value
	 */
	public void addValue(double value){
		data.add(new Double(value));
		if(data.size()==1){
			min = value;
			max = value;
		}else{
			if(value<=min){
				min = value;
			}
			if(value>=max){
				max = value;
			}
		}
		sum += value;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	public double getSum() {
		return sum;
	}
	
	public double getMean(){
		double mean = 0;
		if(data.size()>0){
			mean = sum/data.size();
		}
		return mean;
	}
	
	public int getCount() {
		return data.size();
	}
	
	/**
	 * compute standard deviation estimate
	 * @return the standard deviation estimate
	 */
	public double getSTD(){
		double sigma = 0;
		double value = 0;
		double mean = getMean();
		double sumSQ = 0;
		double n = getCount();
		
		/**
		 * requires at least two values to calculate standard deviation
		 */
		if(n>1){
			Iterator<Double> i = data.iterator();
			while(i.hasNext()){
				value = i.next().doubleValue();
				sumSQ += Math.pow(value-mean,2);
			}
			sigma = Math.sqrt(sumSQ/(n-1));
		}else{
			sigma = 0;
		}
		return sigma; 
	}
	
	/**
	 * self test
	 * @param args
	 */
	public static void main(String args[]) {
		SampleStatistics sample = new SampleStatistics();
		for(int i=0;i<10;i++){
			sample.addValue((double) i);
		}
		System.out.println("Min: " + sample.getMin());
		System.out.println("Mean: " + sample.getMean());
		System.out.println("Max: " + sample.getMax());
		System.out.println("STD: " + sample.getSTD());
		System.out.println("SUM: " + sample.getSum());
		System.out.println("COUNT: " + sample.getCount());
	}
	
	/**
	 * show the statistics in a string
	 */
	public String toString(){
		return "Min: " + getMin() 
				+ "\tMean: " + getMean() 
				+ "\tMax: " + getMax()
				+ "\tSTD: " + getSTD()
				+ "\tSUM: " + getSum()
				+ "\tCOUNT: " + getCount();
	}
	
	/**
	 * sort all values in the vector
	 */
	public void sortValues(boolean decreasing){
		Collections.sort(data);
		if(decreasing){
			Collections.reverse(data);
		}
	}
	
	public double getValue(int index){
		return data.get(index).doubleValue();
	}
	
	/**
	 * get the number of values above the threshold
	 * @param threshold the threshold
	 * @return the number of top values
	 */
	public int getNumValuesAbove(double threshold){
		Iterator<Double> it = data.iterator();
		int c = 0;
		double value = 0.0;
		while(it.hasNext()){
			value = it.next().doubleValue();
			if(value>=threshold){
				c++;
			}
		}
		return c; 
	}
	
	/**
	 * get the maximum value from an array of doubles
	 * @param t
	 * @return
	 */
	public static double max(double[] t) {
	    double maximum = t[0];   // start with the first value
	    for (int i=1; i<t.length; i++) {
	        if (t[i] > maximum) {
	            maximum = t[i];   // new maximum
	        }
	    }
	    return maximum;
	}

}

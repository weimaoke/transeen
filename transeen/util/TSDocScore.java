package transeen.util;

public class TSDocScore implements Comparable<TSDocScore> {

	protected String docID = "";
	protected double docScore = 0.0;
	protected double normScore = 0.0;
	protected double normBase = 10;
	protected double normFactor = 0.0;
	protected String fullPath = "";
	
	public TSDocScore(String id, double score){
		docID = id;
		docScore = score;
	}
	
	public String getID(){
		return docID;
	}
	public double getScore(){
		return docScore;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	
	public void setNormBase(double base){
		normBase = base;
	}
	
	public void normalize(double factor){
		normFactor = factor;
		normScore = docScore * (normBase + normFactor);
	}
	
	public double getNormScore(){
		return normScore;
	}

	public int compareTo(TSDocScore o) {
		int n = 0;
		if(normScore > o.getNormScore()){
			n = 1;
		}else if(normScore == o.getNormScore()){
			n = 0;
		}else{
			n = -1;
		}
		return n;
	}
}

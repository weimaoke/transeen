package transeen.util;

public class TSLinkScore implements Comparable<TSLinkScore>{

	protected String linkID = "";
	protected double linkScore = 0.0;
	protected int linkDegree = 1;
	protected int linkPages = 1;
	protected int linkPick = 1;

	public TSLinkScore(String id, double score){
		linkID = id;
		linkScore = score;
	}
	
	public String getID(){
		return linkID;
	}
	public double getScore(){
		return linkScore;
	}
	
	/**
	 * get topical distance based on score
	 * @return
	 */
	public double getDistance(){
		return 1.0/(linkScore + 0.00001);
	}
	public void setScore(double score){
		linkScore = score;
	}

	public int getDegree(){
		return linkDegree;
	}
	
	/**
	 * normalize a degree value
	 * @param fromMinD the original minimum degree
	 * @param fromMaxD the original maximum degree
	 * @param toMinD the target min degree
	 * @param toMaxD the target max degree
	 * @return the normalized degree
	 */
	public int getNormDegree(int fromMinD,int fromMaxD,int toMinD, int toMaxD){
		int normDegree = (linkDegree - fromMinD) * (toMaxD - toMinD) / (fromMaxD - fromMinD) + toMinD;
		return normDegree;
	}
	
	public void setDegree(int degree){
		linkDegree = degree;
	}
	
	public int getPages() {
		return linkPages;
	}

	public void setPages(int linkPages) {
		this.linkPages = linkPages;
	}

	public int getPick() {
		return linkPick;
	}

	public void setPick(int linkPick) {
		this.linkPick = linkPick;
	}

	@Override
	public int compareTo(TSLinkScore another) {
		// TODO Auto-generated method stub
		double diff = (getScore()-another.getScore());
		int ret = 0;
		if(diff == 0.0){
			ret = 0;
		}else if(diff > 0.0){
			ret = 1;
		}else{
			ret = -1;
		}
		return ret;
	}
}

package transeen.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

public class TSCommon {

	/**
	 * Get a string array of neighbors and return a vector of RefEntries. 
	 * @param sn neighbors in a string array
	 * @return neighbors in a RefEntry vector
	 */
	public static Vector<TSReferenceEntry> getNeighbors(String[] sn){
		Vector<TSReferenceEntry> vrn = new Vector<TSReferenceEntry>();
		if(sn != null){
			for(int i=0;i<sn.length;i++){
				TSReferenceEntry myRef = new TSReferenceEntry(i,sn[i],0.0);
				vrn.add(myRef);
			}
		}
		return vrn;
	}
	
	/**
	 * Get a random integer number BUT number 'n'
	 * @param n the number to be excluded
	 * @param max the max number, i.e., range [0,max]
	 * @return the random integer
	 */
	public static int getRandomBut(int n,int max){
		/**
		 * [0,..,max] has max+1 elements
		 * after n is removed, there should be max elements
		 */
		int[] a = new int[max];
		/**
		 * form the array and skip value 'n'
		 */
		for(int i=0;i<a.length;i++){
			if(i<n){
				a[i] = i;
			}else if(i>n){
				a[i] = i + 1;
			}
		}
		Random rand = new Random(System.currentTimeMillis());
		int r = rand.nextInt(a.length-1);
		return a[r];
	}
	
	/**
	 * Get a random integer
	 * @param max the max number, i.e., range [0,..,max]
	 * @return the random number
	 */
	public static int getRandom(int max){
		Random rand = new Random(System.currentTimeMillis());
		int r = rand.nextInt(max);
		return r;
	}
	
	/**
	 * get a number of random items from a vector
	 * @param vt the input vector of items
	 * @param numItems the number of items to be retrieved
	 * @return the vector of random items picked
	 */
	public static Vector<String> getRandomItems(Vector<String> vt, int numItems){
		// prepare a set of ids for randomization
		Vector<Integer> ids = new Vector<Integer>();
		for(int id=0;id<vt.size();id++){
			ids.add(new Integer(id));
		}
		
		/**
		 *  randomly pick a number (i.e., sampleSize) of agent
		 *  and send them the survey query 
		 */
		Random r = new Random(System.currentTimeMillis());
		Vector<String> items = new Vector<String>();
		for(int k=0;k<numItems;k++){
			/**
			 * randomize an unpicked id
			 */
			int max = ids.size();
			int i = r.nextInt(max);
			int id = ids.get(i).intValue();
			ids.remove(i);
			
			/**
			 * get the item through the id
			 * and add it the picked collection
			 */
			String item = vt.get(id);
			items.add(item);
		}
		return items;
	}
	
	/**
	 * remove an item from a vector
	 * @param item the item to be removed
	 * @param vt a vector where the item might have been
	 */
	public static void removeItem(String item, Vector<String> vt){
		for(int i= (vt.size()-1);i>=0;i--){
			if(vt.get(i).equalsIgnoreCase(item)){
				vt.remove(i);
			}
		}
	}
	
	/**
	 * do a random guess based on a probability value
	 * @param prob a probability value between 0 and 1
	 * @return true (happens) or false (no) 
	 */
	public static boolean guess(double prob){
		if(prob<=0){
			return false;
		}else if(prob>=1){
			return true;
		}else{
			double r = Math.random();
			if(r <= prob){
				return true;
			}else{
				return false;
			}
		}
	}
	
	/**
	 * pick a degree value based on a power law distribution
	 * @param min the min degree
	 * @param max the max degree
	 * @param exp the exponent of the power law
	 * @return a degree value
	 */
	public static int pickPowerLawDegree(int min,int max,double exp){
		double p[] = new double[max-min+1];
		double p2[] = new double[p.length+1];
		double sumP = 0.0;
		
		/**
		 * compute the probability values
		 */
		for(int i=0;i<p.length;i++){
			p[i] = Math.pow(min+i,0.0 - exp);
			sumP += p[i];
		}
		p2[0] = 0.0;
		for(int i=1;i<p2.length;i++){
			p2[i] = p2[i-1] + p[i-1]/sumP;
		}
		
		/**
		 * pick a random number and find the degree range
		 */
		double r = Math.random();
		int degree = min + TSCommon.binarySearch(p2,r);
		return degree;
	}
	
	/**
     * Performs the standard binary search
     * using two comparisons per level.
     * @return index where item is found, or NOT_FOUND.
     */
    public static int binarySearch(double[] a, double x )
    {
        int low = 0;
        int high = a.length - 2;
        int mid;

        while(low <= high){
            mid = ( low + high ) / 2;

            if(x > a[mid+1])
                low = mid + 1;
            else if(x < a[mid])
                high = mid - 1;
            else
                return mid;
        }
        return 0;
    }
    
    /**
     * get current date time in a string
     * @return the string of time
     */
    public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
}

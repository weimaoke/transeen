package transeen.util;

public class TSTermFrequency implements Comparable<TSTermFrequency> {

	protected String term;
	protected int freq;
	public TSTermFrequency(String t,int f){
		term = t;
		freq = f;
	}
	
	public String getTerm(){
		return term;
	}
	
	public int getFrequency(){
		return freq;
	}

	@Override
	public int compareTo(TSTermFrequency aTF) {
		return (getFrequency() - aTF.getFrequency());
	}

}

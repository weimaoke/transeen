package transeen;

import transeen.util.OptionLoader;

import transeen.util.TextReadWriter;
import transeen.database.*;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator; //import java.util.Vector;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

//import jade.wrapper.StaleProxyException;

public class MainApp {
	protected static OptionLoader optionLoader = null;
	protected static DatabaseFactory db = null;

	/**
	 * Start the program...
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		Runtime rt = Runtime.instance();
		rt.setCloseVM(true);

		optionLoader = new OptionLoader(null);
		/**
		 * Set default values for host and port and read arguments from command
		 * line.
		 */
		String node = "0";
		String mainHost = "localhost";
		String mainPort = "1099";
		String localHost = "localhost";
		String localPort = "1100";
		String containerPrefix = "LAIR";
		int rewireStart = 0;
		int rewireAddRandomNeighbors = 20;

		mainHost = optionLoader.getOptionString("mainHost");
		mainPort = optionLoader.getOptionString("mainPort");
		localHost = optionLoader.getOptionString("localHost");
		localPort = optionLoader.getOptionString("localPort");
		containerPrefix = optionLoader.getOptionString("containerPrefix");
		rewireStart = optionLoader.getOptionInt("rewireStart");
		rewireAddRandomNeighbors = optionLoader
				.getOptionInt("rewireAddRandomNeighbors");

		// NODE
		if (args.length > 0) {
			node = args[0];
			node = node.trim();
			if (node.equals("")) {
				node = "0";
			}
		}

		// LOCALHOST
		if (args.length > 1) {
			localHost = args[1];
		}

		// LOCALHOST
		if (args.length > 2) {
			localPort = args[2];
		}

		// Start the experiment or not
		int startExp = optionLoader.getOptionInt("startExperiment");
		if (args.length > 3) {
			startExp = new Integer(args[3]).intValue();
		} else {
			startExp = 0;
		}

		/**
		 * Create a new main container
		 */
		Profile p = new ProfileImpl();
		p.setParameter(Profile.PLATFORM_ID, "TRANSEEN");
		p.setParameter(Profile.MAIN_HOST, mainHost);
		p.setParameter(Profile.MAIN_PORT, mainPort);
		//p.setParameter(Profile.MASTER_NODE_NAME, containerPrefix);
		p.setParameter(Profile.ACCEPT_FOREIGN_AGENTS, "true");
		// jade_core_messaging_MessageManager_maxqueuesize
		ContainerController ac = null;
		/**
		 * Parameter
		 */
		if (node.equals("0")) {
			/**
			 * node 0 deletes the DONE file, which keeps track of the status of
			 * experiments
			 */
			File doneFile = new File(optionLoader.getOverallDoneFileName());
			if (doneFile.exists()) {
				try {
					if (doneFile.delete()) {
						System.out.println("DEBUG\t"
								+ optionLoader.getOverallDoneFileName()
								+ "\thas been deleted.");
					} else {
						System.out.println("DEBUG\t"
								+ optionLoader.getOverallDoneFileName()
								+ "\thas NOT been deleted.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			p.setParameter("jade_core_messaging_MessageManager_maxqueuesize",
					optionLoader.getOptionString("messageManagerMaxSize"));
			p.setParameter(Profile.CONTAINER_NAME, containerPrefix);
			p.setParameter(Profile.LOCAL_HOST, mainHost);
			p.setParameter(Profile.LOCAL_PORT, mainPort);
			p.setParameter(Profile.MAIN, "true");
			ac = rt.createMainContainer(p);
		} else {
			p.setParameter("jade_core_messaging_MessageManager_maxqueuesize",
					optionLoader.getOptionString("messageManagerMaxSize"));
			p.setParameter(Profile.CONTAINER_NAME, containerPrefix + node);
			p.setParameter(Profile.LOCAL_HOST, localHost);
			p.setParameter(Profile.LOCAL_PORT, localPort);
			p.setParameter(Profile.MAIN, "false");
			ac = rt.createAgentContainer(p);
		
			/**
			 * Create new agents
			 */
			try {
				/**
				 * start the agent container 
				 */
				//ac.start();
				
				/**
				 * start agents
				 */
				int endSID = startAgents(node, ac);
				int numAgents = optionLoader.getOptionInt("numAgents");			
				
				// Load query agent to start experiment
				AgentController agent = null;
				if (optionLoader.getOptionInt("startExperiment") > 0 && endSID >= numAgents) {
					// Wait for a while for all agents to initialize
					int t = optionLoader.getOptionInt("queryAgentWait");
					Thread.sleep(t*1000);
					agent = ac.createNewAgent("ALLQUERY", "transeen.TSQueryAgent", null);
					agent.start();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("JADE Profile:\n" + p.toString());
	}

	/**
	 * start agents from file
	 * @return the ID/number of the last agent created
	 */
	private static int startAgents(String node, ContainerController ac) {
		int endSID = 0;
		try {
			int nodeID = new Integer(node).intValue();
			String tbDomains = optionLoader.getOptionString("tbDomains");
			db = new DatabaseFactory();

			// Get summary information of all domains
			db.connect();
			PreparedStatement st = db
					.prepareStatement("select min(degree) as mind,max(degree) as maxd,count(sid) as numAgents from " + tbDomains);
			ResultSet rs = st.executeQuery();
			int minD = 1, maxD = 2, numAgents = 100;
			if (rs.next()) {
				minD = rs.getInt("mind");
				maxD = rs.getInt("maxd");
				numAgents = rs.getInt("numAgents");
			}

			int rewireMinNumNeighbors = optionLoader
					.getOptionInt("rewireMinNumNeighbors");
			int rewireMaxNumNeighbors = optionLoader
					.getOptionInt("rewireMaxNumNeighbors");

			// retrieve individual domain/agent information
			AgentController agent = null;
			String agentName = null;
			int agentDegree = 1;
			int agentNumPages = 1;
			int agentSID = 0;

			int numAgentsPerNode = optionLoader
					.getOptionInt("numAgentsPerNode");
			int startSID = numAgentsPerNode * (nodeID - 1) + 1;
			endSID = numAgentsPerNode * nodeID;
			st = db.prepareStatement("select domain, degree, numpages,sid from "
					+ tbDomains + " where sid>=? and sid<=?");
			st.setInt(1, startSID);
			st.setInt(2, endSID);
			rs = st.executeQuery();
			while (rs.next()) {
				agentName = rs.getString("domain");
				agentDegree = rs.getInt("degree");
				agentDegree = (agentDegree - minD)
						* (rewireMaxNumNeighbors - rewireMinNumNeighbors)
						/ (maxD - minD) + rewireMinNumNeighbors;
				agentNumPages = rs.getInt("numpages");
				agentSID = rs.getInt("sid");
				//System.out.println(agentName + "\t" + agentDegree + "\t" + agentNumPages);
				Integer[] paras = new Integer[5];
				paras[0] = new Integer(agentSID);
				paras[1] = new Integer(agentDegree);
				paras[2] = new Integer(agentNumPages);
				paras[3] = new Integer(minD);
				paras[4] = new Integer(maxD);
				if (agentName != null && !agentName.equalsIgnoreCase("")) {
					agent = ac.createNewAgent(agentName, "transeen.TSAgent", paras);
					agent.start();
				}
			}
			db.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return endSID;
	}
}

/**
 * 
 */
package transeenlightUNC;

import java.io.BufferedWriter;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import transeen.database.DatabaseFactory;
import transeen.lucene.TSIndexReader;
import transeen.lucene.TSLinkSearcher;
import transeen.lucene.TSLocalSearcher;
import transeen.lucene.TSMetaIndexer;
import transeen.util.OptionLoader;
import transeen.util.TSCommon;
import transeen.util.TSDocScore;
import transeen.util.TSLinkScore;
import transeen.util.TSReferenceEntry;
import transeen.util.TSQuery;
import transeen.util.SampleStatistics;
import transeen.util.TextReadWriter;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;
import org.apache.lucene.wordnet.AnalyzerUtil;

/**
 * A Referral Network of Agents for Expertise/Service Discovery
 * 
 * @author Weimao Ke
 * @author Laboratory of Applied Informatics Research
 * @author University of North Carolina at Chapel Hill
 * @version 0.1
 * 
 */
public class TSQueryAgentLight extends Agent {

	/**
	 * Generate serial ID
	 */
	private static final long serialVersionUID = 1821959431399944318L;

	/**
	 * logWriter to keep track of the experimental results
	 */
	BufferedWriter logWriter = null;
	BufferedWriter chainWriter = null;
	BufferedWriter surveyWriter = null;
	BufferedWriter doneWriter = null;
	BufferedWriter summaryWriter = null;

	/**
	 * Experimental settings
	 */
	protected double[] expClusteringExponents;
	protected int indClusteringExponent = 0;
	protected int[] minNeighbors;
	protected int[] maxNeighbors;
	protected int indNeighbors = 0; 
	protected String[] expSearchMethods;
	protected int indSearchMethod = 0;
	protected int[] expMaxSearchLengths;
	protected int indMaxSearchLength = 0;
	protected int expMoreRelevantSearchLength = 0;
	protected int topRelevantAgents = 1;
	protected int topRelevantDocs = 1;
	protected int topRelevantDocsAll = 1;
	protected int retRelevantDocsAll = 1;
	protected double scoreNormBase = 10;


	/**
	 * Various handlers the agent has: an option loader, a data loader, an
	 * information organizer, and a neighbor mapper.
	 */
	protected DatabaseFactory db = null;
	protected OptionLoader optionLoader = null;

	protected int queryIndex = 0;
	protected int maxNumQueries = 5;
	protected int queryNumReturned = 0;
	protected double expertScore = 0.5;

	/**
	 * variables to summarize the final results
	 */
	protected int queryWorking = 0;
	protected int queryFailed = 0;
	protected int querySucceeded = 0;
	SampleStatistics expertScoreStatistics = null;
	SampleStatistics expertsInSampleStatistics = null;
	SampleStatistics bestScoreStatistics = null;
	SampleStatistics degreeStatistics = null;

	/**
	 * variables for summary results
	 */
	// Score\tSearchLength\tSearchTime\tP\tR\tF\nDCG
	protected int rsSuccessCount = 0;
	protected double rsScoreSum = 0.0;
	protected long rsSearchLengthSum = 0;
	protected long rsSearchTimeSum = 0;
	protected double rsPSum = 0.0;
	protected double rsRSum = 0.0;
	protected double rsFSum = 0.0;
	protected double rsNDCGSum = 0.0;
	protected double rsQueryCount = 0.0;

	/**
	 * names of all agents on all nodes
	 */
	protected String[] allAgents;
	/**
	 * names of first degree agents on all nodes
	 */
	protected String[] firstAgents;

	/**
	 * references to queries (query docs)
	 */
	protected String[] allQueryIDs;
	protected String[] allQueryDomains;

	protected double localSearchThreshold = 0.5;
	protected int localSearchTopDocs = 10;

	/**
	 * variables on status of the current training query
	 */
	protected int expertSurvey = 1;
	protected int surveySampleSize = 50;
	protected int surveyExpertNum = 5;
	protected SampleStatistics surveySample = null;
	protected double surveyExpertSTD = 1;

	protected String surveyQueryID = "";
	protected int surveyQueryIndex = 0;
	protected String surveyBestNeighbor = "";
	protected int surveyBestNeighborID = 0;
	protected double surveyBestScore = 0.0;
	protected int surveyNumSent = 0;
	protected int surveyNumReturned = 0;

	/**
	 * Initialization of the agent.
	 */
	protected void setup() {
		printVersion();
		/**
		 * Initialize the handlers.
		 */
		optionLoader = new OptionLoader(null);

		try {

			maxNumQueries = optionLoader.getOptionInt("maxNumQueries");
			surveySampleSize = optionLoader.getOptionInt("surveySampleSize");
			surveyExpertNum = optionLoader.getOptionInt("surveyExpertNum");
			expertSurvey = optionLoader.getOptionInt("expertSurvey");
			expertScore = optionLoader.getOptionDouble("classifierExpertScore");
			surveyExpertSTD = optionLoader.getOptionDouble("surveyExpertSTD");

			localSearchThreshold = optionLoader
					.getOptionDouble("localSearchThreshold");
			localSearchTopDocs = optionLoader
					.getOptionInt("localSearchTopDocs");

			/**
			 * initialize the summary table
			 */
			queryWorking = 0;
			queryFailed = 0;
			querySucceeded = 0;
			expertScoreStatistics = new SampleStatistics();
			expertsInSampleStatistics = new SampleStatistics();
			bestScoreStatistics = new SampleStatistics();
			degreeStatistics = new SampleStatistics();

			/**
			 * Load all agent names to an array Load all queries to an array
			 */
			initAgentQueries();

			System.out.println("Hello! QueryAgent " + getAID().getName()
					+ " is ready.");

			/**
			 * print log heading
			 */
			logWriter = TextReadWriter.writerFile(optionLoader
					.getExperimentLogName(allAgents.length)
					+ OptionLoader.LOG_FILE_SUFFIX);
			chainWriter = TextReadWriter.writerFile(optionLoader
					.getExperimentLogName(allAgents.length)
					+ OptionLoader.CHAIN_FILE_SUFFIX);
			TextReadWriter
					.writeLine(
							logWriter,
							"Alpha\tMinNeighbors\tMaxNeighbors\tMaxSearchLength\tSearchMethod\tID\tQuery\tBestAgent\tBestDoc\tBestScore"
									+ "\tStatus\tStatusName\tSearchLength\tSearchTime\tP\tR\tF\tnDCG");
			String summaryFile = optionLoader
					.getOverallSummaryFileName(allAgents.length);
			if ((new File(summaryFile)).exists()) {
				summaryWriter = TextReadWriter.writerFile(summaryFile, true);
			} else {
				summaryWriter = TextReadWriter.writerFile(summaryFile);
				TextReadWriter
						.writeLine(
								summaryWriter,
								"TIME\tAlpha\tMinNeighbors\tMaxNeighbors\tMaxSearchLength\tSearchMethod\tQueryCount\t"
										+ "Success\tScore\tSearchLength\tSearchTime\tP\tR\tF\tnDCG");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * Begin monitoring incoming messages
		 */
		addBehaviour(new MessageMonitor());

		/**
		 * Begin queries
		 */
		initExpSettings();
		double alpha = expClusteringExponents[indClusteringExponent];
		int minN = minNeighbors[indNeighbors];
		int maxN = maxNeighbors[indNeighbors]; 
		String searchMethod = expSearchMethods[indSearchMethod];
		int maxL = expMaxSearchLengths[indMaxSearchLength];
		nextQuery(alpha, minN, maxN, searchMethod, maxL, localSearchThreshold);
	}
	
	protected void printVersion(){
		System.out.println("Transeen major version: 2010.02.04");
	}

	/**
	 * initialize experiment setting variables
	 */
	protected void initExpSettings() {
		/**
		 * experimental settings
		 */
		expClusteringExponents = optionLoader.getOptionDoubleArray(
				"expClusteringExponents", " ");
		expSearchMethods = optionLoader.getOptionStringArray(
				"expSearchMethods", " ");
		expMaxSearchLengths = optionLoader.getOptionIntArray(
				"expMaxSearchLengths", " ");
		indClusteringExponent = 0;
		indSearchMethod = 0;
		indMaxSearchLength = 0;
		
		minNeighbors = optionLoader.getOptionIntArray("rewireMinNumNeighbors"," ");
		maxNeighbors = optionLoader.getOptionIntArray("rewireMaxNumNeighbors"," "); 
		indNeighbors = 0; 
		
		expMoreRelevantSearchLength = optionLoader.getOptionInt("expMoreRelevantSearchLength");
		topRelevantAgents = optionLoader.getOptionInt("topRelevantAgents");
		topRelevantDocs = optionLoader.getOptionInt("topRelevantDocs");
		topRelevantDocsAll = optionLoader.getOptionInt("topRelevantDocsAll");
		retRelevantDocsAll = optionLoader.getOptionInt("retRelevantDocsAll");
		scoreNormBase = optionLoader.getOptionDouble("scoreNormBase");
	}

	/**
	 * set the next experimental setting indexes
	 * 
	 * @return true if succeeded; false, if all experimental settings have been
	 *         used.
	 */
	protected boolean nextExpSetting() {
		boolean next = true;
		if (indSearchMethod < (expSearchMethods.length - 1)) {
			indSearchMethod++;
		} else {
			indSearchMethod = 0;
			if (indMaxSearchLength < (expMaxSearchLengths.length - 1)) {
				indMaxSearchLength++;
			} else {
				indMaxSearchLength = 0;
				if(indNeighbors < (minNeighbors.length - 1)){
					indNeighbors++;
				}else{
					indNeighbors = 0; 
					if (indClusteringExponent < (expClusteringExponents.length - 1)) {
						indClusteringExponent++;
					} else {
						next = false;
					}
				}
			}
			
		}
		System.out.println("\nAlpha: "
				+ expClusteringExponents[indClusteringExponent]
				+ "; Neighbors: ["
				+ minNeighbors[indNeighbors] + ", " + maxNeighbors[indNeighbors] + "]"
				+ "; Max Search Length: "
				+ expMaxSearchLengths[indMaxSearchLength] 
				+ "; Search Method: "
				+ expSearchMethods[indSearchMethod] + ".");
		return next;
	}

	/**
	 * Before shutting down the agent.
	 */
	protected void takeDown() {
		/**
		 * Set major object references to nulls.
		 */
		optionLoader = null;

		try {
			if (logWriter != null) {
				logWriter.close();
				logWriter = null;
			}
			if (chainWriter != null) {
				chainWriter.close();
				chainWriter = null;
			}
			if (surveyWriter != null) {
				surveyWriter.close();
				surveyWriter = null;
			}
			if (summaryWriter != null) {
				summaryWriter.close();
				summaryWriter = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("RefAgent " + getAID().getName()
				+ " is to take off.");
	}

	/**
	 * Initiate agent and query lists
	 */
	protected void initAgentQueries() {
		try {
			db = new DatabaseFactory();
			db.connect();
			String tbDomains = optionLoader.getOptionString("tbDomains");
			String tbQueries = optionLoader.getOptionString("tbQueries");

			// retrieve the list of agents
			int numAgents = optionLoader.getOptionInt("numAgents");
			if (numAgents > 0) {
				allAgents = new String[numAgents];
				firstAgents = allAgents;
			}

			PreparedStatement st = db.prepareStatement("select domain from "
					+ tbDomains);
			ResultSet rs;
			rs = st.executeQuery();
			int index = 0;
			while (rs.next() && index < numAgents) {
				allAgents[index] = rs.getString("domain");
				index++;
			}

			// retrieve the list of queries
			// numQueries = optionLoader.getOptionInt("maxNumQueries");
			if (maxNumQueries > 0) {
				allQueryIDs = new String[maxNumQueries];
				allQueryDomains = new String[maxNumQueries];
			}

			st = db.prepareStatement("select domain,trecid from " + tbQueries);
			rs = st.executeQuery();
			index = 0;
			while (rs.next() && index < maxNumQueries) {
				String domain = rs.getString("domain");
				String trecid = rs.getString("trecid");
				allQueryIDs[index] = trecid;
				allQueryDomains[index] = domain;
				index++;
			}
			maxNumQueries = index;
			rs.close();
			st.close();
			db.disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Send a query (with a query data instance) to a receiver.
	 * 
	 * @param receiver
	 *            the one who is to receive the query
	 * @param query
	 *            the query instance (RefQuery)
	 */
	protected void sendQuery(String receiver, TSQuery query) {
		/**
		 * set sender and receiver information in query
		 */
		query.setCurrentSender(getLocalName());
		query.setCurrentReceiver(receiver);
		
		/**
		 * create a new message and send it
		 */
		ACLMessage msg = new ACLMessage(ACLMessage.QUERY_IF);
		msg.setLanguage("English");
		msg.setOntology("Transeen-Ontology");
		
		try {
			msg.setContentObject(query);
		} catch (IOException e) {
			e.printStackTrace();
		}
		msg.addReceiver(new AID("ALLAGENT", AID.ISLOCALNAME));
		send(msg);
		System.out.print(">> " + receiver + " ");
	}

	/**
	 * Assign the next query to an agent.
	 */
	public boolean nextQuery(double alpha, int minNgbs, int maxNgbs, String searchMethod, int maxSearchL,
			double newThresholdScore) {
		boolean querySent = false;
		if (queryIndex < allQueryIDs.length) {

			TSQuery newQuery = new TSQuery();
			newQuery.setSid(queryIndex);
			newQuery.setID("Q" + queryIndex);
			newQuery.setThresholdScore(newThresholdScore);
			newQuery.addRef(new TSReferenceEntry(getLocalName(), 0.0));

			// Load query content
			String trecid = allQueryIDs[queryIndex];
			String domain = allQueryDomains[queryIndex];
			System.out.print("\nQuery " + queryIndex + "(" + domain + "/"
					+ trecid + "): ALLQUERY ");
			String domainPath = optionLoader.getAgentDomainDataPath(domain);
			String fileName = domainPath + "/" + trecid + ".txt";
			String queryString = "";
			if (new File(fileName).exists()) {
				try {
					queryString = TextReadWriter.readText(fileName);
					queryString = queryString.replaceAll(
							"[\"\',\\.\\[\\]\\(\\);]", " ");
					int numTerms = optionLoader
							.getOptionInt("maxNumQueryTerms");
					String[] terms = AnalyzerUtil.getMostFrequentTerms(
							new StandardAnalyzer(Version.LUCENE_CURRENT),
							queryString, numTerms);
					queryString = "";
					for (int i = 0; i < terms.length; i++) {
						String term = terms[i].replaceAll("\\d+:", "");
						queryString += " " + term;
					}
					/**
					 * String indexDir =
					 * optionLoader.getAgentDomainIndexPath(domain);
					 * TRECWebIndexReader wIR = new
					 * TRECWebIndexReader(indexDir); int numTerms =
					 * optionLoader.getOptionInt("maxNumQueryTerms");
					 * System.out.println(indexDir + "\t" + trecid + "\t" +
					 * numTerms); queryString = wIR.readDocKeyTerms(trecid,
					 * numTerms);
					 */

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (queryString != null && (!queryString.equals(""))
						&& (!queryString.equals(" "))) {
					newQuery.setID(trecid);
					newQuery.setDomain(domain);
					newQuery.setQuery(queryString);
					newQuery.setAlpha(alpha);
					newQuery.setMinNeighbors(minNgbs);
					newQuery.setMaxNeighbors(maxNgbs); 
					newQuery.setSearchMethod(searchMethod);
					newQuery.setMaxSearchLength(maxSearchL);
					newQuery.setMoreRelevantSearchLength(expMoreRelevantSearchLength);
					newQuery.setAsRequest();
					newQuery.setStartSearchTime();
					String name = randomAgent(firstAgents);
					// String name = "lyricsmania.com";
					sendQuery(name, newQuery);
					querySent = true;
				} else {
					querySent = false;
				}
			} else {
				querySent = false;
			}
			queryIndex++;
		}
		return querySent;
	}

	/**
	 * randomly pick an agent as the first agent for querying
	 * 
	 * @param agents
	 *            the list of agents
	 */
	public String randomAgent(String[] agents) {
		String name = null;
		if (agents != null) {
			Random r = new Random();
			int i = r.nextInt(agents.length);
			name = agents[i];
		}
		return name;
	}

	/**
	 * save exact match results to log files
	 * @param query the returned query
	 * @param exactMatch exact match or relevance search
	 */
	protected void recordResults(TSQuery query,boolean exactMatch){
		double recall = 0, precision = 0, F1 = 0, nDCG = 0;
		if(!exactMatch){
			System.out.println("Relevant search -- not exact match");
			computeRelevantResults(query,topRelevantAgents,topRelevantDocs);
		}
		recall = query.getRecall();
		precision = query.getPrecision();
		F1 = query.getF1();
		nDCG = query.getnDCG();
		try {
			/**
			 * log detailed results
			 */
			// Alpha\tMaxSearchLength\tSearchMethod\tID\tQuery\tBestAgent\tBestDoc\tBestScore\tStatus\tSearchLength\tSearchTime\tP\tR\tF\nDCG
			TextReadWriter.writeLine(logWriter, query
					.getAlpha()
					+ "\t"
					+ query.getMinNeighbors()
					+ "\t"
					+ query.getMaxNeighbors()
					+ "\t"
					+ query.getMaxSearchLength()
					+ "\t"
					+ query.getSearchMethod()
					+ "\t"
					+ query.getSid()
					+ "\t"
					+ query.getID()
					+ "\t"
					+ query.getBestAgent()
					+ "\t"
					+ query.getBestDoc()
					+ "\t"
					+ query.getBestScore()
					+ "\t"
					+ query.getStatus()
					+ "\t"
					+ query.getStatusString()
					+ "\t"
					+ query.getNumRefEntries()
					+ "\t"
					+ query.getSearchTime()
					+ "\t"
					+ precision
					+ "\t"
					+ recall
					+ "\t"
					+ F1
					+ "\t"
					+ nDCG);

			TextReadWriter
					.writeLine(
							chainWriter,
							"CN\t"
									+ expClusteringExponents[indClusteringExponent]
									+ "\t"
									+ expMaxSearchLengths[indMaxSearchLength]
									+ "\t"
									+ expSearchMethods[indSearchMethod]
									+ "\t"
									+ query
											.getRefEntryNames());
			TextReadWriter
					.writeLine(
							chainWriter,
							"CS\t"
									+ expClusteringExponents[indClusteringExponent]
									+ "\t"
									+ expMaxSearchLengths[indMaxSearchLength]
									+ "\t"
									+ expSearchMethods[indSearchMethod]
									+ "\t"
									+ query
											.getRefEntryScores());

			/**
			 * prepare summary results
			 */
			// TIME\tAlpha\tMaxSearchLength\tSearchMethod\tQueryCount\tSuccess\tScore\tSearchLength\tSearchTime\tP\tR\tF\nDCG\n
			rsQueryCount++;
			if (query.getStatus() == TSQuery.STATUS_SUCCEEDED) {
				rsSuccessCount++;
			}
			rsScoreSum += query.getBestScore();
			rsSearchLengthSum += query.getNumRefEntries();
			rsSearchTimeSum += query.getSearchTime();
			rsPSum += query.getPrecision();
			rsRSum += query.getRecall();
			rsFSum += query.getF1();
			rsNDCGSum += query.getnDCG();

		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * when queries for one experimental setting are
		 * done
		 */
		if (queryIndex >= maxNumQueries) {
			/**
			 * log summary results
			 */
			// TIME\tAlpha\tMaxSearchLength\tSearchMethod\tQueryCount\tSuccess\tScore\tSearchLength\tSearchTime\tP\tR\tF\nDCG\n
			try {
				TextReadWriter
						.writeLine(
								summaryWriter,
								TSCommon.getDateTime()
										+ "\t"
										+ expClusteringExponents[indClusteringExponent]
										+ "\t"
										+ minNeighbors[indNeighbors]
										+ "\t"
										+ maxNeighbors[indNeighbors]
										+ "\t"
										+ expMaxSearchLengths[indMaxSearchLength]
										+ "\t"
										+ expSearchMethods[indSearchMethod]
										+ "\t"
										+ rsQueryCount
										+ "\t"
										+ rsSuccessCount
												* 1.0
												/ rsQueryCount
										+ "\t"
										+ rsScoreSum
												/ rsQueryCount
										+ "\t"
										+ rsSearchLengthSum
												* 1.0
												/ rsQueryCount
										+ "\t"
										+ rsSearchTimeSum
												* 1.0
												/ rsQueryCount
										+ "\t"
										+ rsPSum
												/ rsQueryCount
										+ "\t"
										+ rsRSum
												/ rsQueryCount
										+ "\t" + rsFSum
										/ rsQueryCount
										+ "\t" + rsNDCGSum
										/ rsQueryCount
										+ "\t");
			} catch (Exception e) {
				e.printStackTrace();
			}

			/**
			 * go to next experimental setting
			 */
			if (nextExpSetting()) {
				queryIndex = 0;
				rsQueryCount = 0;
				rsSuccessCount = 0;
				rsScoreSum = 0;
				rsSearchLengthSum = 0;
				rsSearchTimeSum = 0;
				rsPSum = 0;
				rsRSum = 0;
				rsFSum = 0;
				rsNDCGSum = 0;
			} else {
				System.out
						.println("=============== ALL IS DONE ==============");
			}
		}

	}
	
	protected void computeRelevantResults(TSQuery query,int topNAgents,int topNDocs){
		TSReferenceEntry[] domains = query.getBestAgents(topNAgents);
		String tmpData = optionLoader.getOptionString("tmpData");
		String tmpIndex = optionLoader.getOptionString("tmpIndex");
		String tbCentral = optionLoader.getOptionString("tbCentral");
		
		/**
		 * remove all data and index files first
		 */
		TextReadWriter.removeAllFiles(tmpData);
		TextReadWriter.removeAllFiles(tmpIndex);
		
		/**
		 * link best agent metadocuments
		 */
		for(int i=0;i<domains.length;i++){
			TSReferenceEntry myDomain = domains[i];
			if(myDomain==null){
				break;
			}else{
				linkMetaDocument(tmpData,myDomain.getAgent());
			}
		}
		
		/**
		 * build index on the link metadocuments
		 */
		try {
			TSMetaIndexer.buildIndex(tmpIndex, tmpData);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		/**
		 * search metadocuments and then search individual domains
		 * and rank the results together
		 */
		Vector<TSDocScore> allRelevantDocs = new Vector<TSDocScore>();
		try {
			Vector<TSLinkScore> vlinks = TSLinkSearcher.search(tmpIndex, query.getQuery(), topNAgents);
			Iterator<TSLinkScore> ilinks = vlinks.iterator();
			while(ilinks.hasNext()){
				TSLinkScore mylink = ilinks.next();
				String linkIndex = optionLoader.getAgentDomainIndexPath(mylink.getID());
				Vector<TSDocScore> vdocs = TSLocalSearcher.search(linkIndex, query.getQuery(), topNDocs, 0.0);
				Iterator<TSDocScore> idocs = vdocs.iterator();
				while(idocs.hasNext()){
					TSDocScore mydoc = idocs.next();
					mydoc.setNormBase(scoreNormBase);
					mydoc.normalize(mylink.getScore());
					allRelevantDocs.add(mydoc);
				}
			}
			
			System.out.println("Total retrieved documents: " + allRelevantDocs.size());
			
			/**
			 * rank the documents and retrieve the best
			 */
			DatabaseFactory db = new DatabaseFactory();
			db.connect();
			String sql = "select count(dtrecid) as d,avg(dscore) as s from " 
				+ "(select dtrecid,dscore from " + tbCentral + " where qsid=" + (query.getSid()+1) + " " 
				+ "order by dscore desc limit " + topRelevantDocsAll + ") as t1 "
				+ "where dtrecid=?";
			System.out.println(sql);
			PreparedStatement stmt = db.prepareStatement(sql);
			Comparator<TSDocScore> c = Collections.reverseOrder();
			Collections.sort(allRelevantDocs, c);
			Iterator<TSDocScore> it = allRelevantDocs.iterator();
			int relCount = 0;
			int i = 0;
			double myScore = 0.0;
			double myDCG = 0.0;	
			double myPos = 0.0;
			for(i=0;i<retRelevantDocsAll;i++){
				if(it.hasNext()){
					TSDocScore mydoc = it.next();
					myPos++;
					if(mydoc!=null){
						System.out.print("RS\t" + query.getID() + "\t" 
								+ mydoc.getID() + "\t" + mydoc.getScore() + "\t" + mydoc.getNormScore());
						if(stmt == null){
							stmt = db.prepareStatement(sql);
						}
						stmt.setString(1,mydoc.getID());
						ResultSet rs = stmt.executeQuery();
						int rel = 0;
						if(rs.next()){
							rel = rs.getInt(1);
							myScore = rs.getDouble(2);
							if(rel>0){
								relCount++;
								if(myPos<=1.0){
									myDCG = myScore;
								}else{
									myDCG += myScore/(Math.log(myPos)/Math.log(2));
								}									
								System.out.print("\tRL\t" + relCount + "\n");
							}else{
								System.out.print("\tNR\t" + relCount + "\n");
							}
						}
					}else{
						break;
					}
				}else{
					break;
				}
			}
			
			// get ideal DCG to normalize DCG value
			String sql2 = "select dtrecid as d, dscore as s from " + tbCentral 
			+ " where qsid=" + (query.getSid()+1) 
			+ " order by dscore desc limit " + myPos;
			PreparedStatement stmt2 = db.prepareStatement(sql2);
			ResultSet rs2 = stmt2.executeQuery();
			double iScore = 0.0;
			double iDCG = 0.0;
			double iPos = 0.0;
			while(rs2.next()){
				iPos++;
				iScore = rs2.getDouble(2);
				if(iPos<=1.0){
					iDCG = iScore;
				}else{
					iDCG += iScore/(Math.log(iPos)/Math.log(2.0));
				}
			}
			
			double p = (relCount*1.0)/(i*1.0);
			double r = (relCount*1.0)/(topRelevantDocsAll);
			double nDCG = 0.0;
			if(iDCG==0.0){
				nDCG = 1.0;
			}else{
				nDCG = myDCG/iDCG;
			}
			query.setPrecision(p);
			query.setRecall(r);
			query.setnDCG(nDCG);
			
			//stmt = null;
			//db.disconnect();
		} catch(SQLException se){
			 se.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	protected void linkMetaDocument(String linkDir, String metaDomain) {
		String metaFile = optionLoader.getAgentDomainMetaPathName(metaDomain);
		String linkFile = linkDir + "/" + metaDomain + ".txt";
		try {
			TextReadWriter.copyText(metaFile, linkFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A private class used to continuously check incoming messages.
	 * 
	 * @author Weimao Ke
	 * 
	 */
	private class MessageMonitor extends CyclicBehaviour {

		/**
		 * Generated serial ID
		 */
		private static final long serialVersionUID = -4360794235242310963L;

		/**
		 * The action loop for checking received messages.
		 */
		@Override
		public void action() {
			ACLMessage msg = myAgent.receive();
			if (msg != null) {
				String ont = msg.getOntology();
				/**
				 * to deal with TSQUERY
				 */
				if (ont != null && ont.equals("Transeen-Ontology")) {
					TSQuery query;
					try {
						query = (TSQuery) msg.getContentObject();
						if (query.isResponse()) {
							query.setEndSearchTime();
							if(expMoreRelevantSearchLength > 0){
								System.out.println("MoreRelevantSearchLength > 0 -- relevant search");
								recordResults(query,false);
							}else{
								System.out.println("MoreRelevantSearchLength <= 0 -- exact search");
								recordResults(query,true);
							}
							/**
							 * next query for each experimental setting
							 */
							boolean success = false;
							while (queryIndex < maxNumQueries && !success) {
								// System.out.print("Query " + queryIndex);
								double alpha = expClusteringExponents[indClusteringExponent];
								int minN = minNeighbors[indNeighbors];
								int maxN = maxNeighbors[indNeighbors]; 
								String searchMethod = expSearchMethods[indSearchMethod];
								int maxL = expMaxSearchLengths[indMaxSearchLength];
								success = nextQuery(alpha, minN, maxN, searchMethod, maxL,
										localSearchThreshold);
							}
						}
					} catch (UnreadableException e) {
						e.printStackTrace();
						System.err.println(getLocalName() + " (<< " + msg.getSender().getName() + "): " + msg.toString());
					}
					msg = null;
				} else if (ont != null) {
					/** non-null non-TSQUERY */
					System.out.println(getLocalName() + " (<<"
							+ msg.getSender().getName() + "): [" + ont
							+ "], " + msg.toString());
				} else {
					System.out.println(getLocalName() + " (<<"
							+ msg.getSender().getName() + "): "
							+ msg.toString() + "");
				}
			} else {
				/**
				 * This somehow skips unnecessary loops when there is no
				 * incoming message.
				 */
				block();
			}
		}

		/**
		 * Show results back from the agents.
		 * 
		 * @param msg
		 *            the message routed back
		 */
		public void showResults(ACLMessage msg) {
			try {
				System.out.println("MSG\t" + msg.getSender().getLocalName());
				TSQuery query = (TSQuery) msg.getContentObject();
				System.out.println("AQ\t"
						+ query.getID()
						+ "\t"
						+ query.getThresholdScore()
						+ "\t"
						+ surveySample.getNumValuesAbove(query
								.getThresholdScore()) + "\t"
						+ query.getBestAgent() + "\t" + query.getBestScore()
						+ "\t" + query.getRefCount() + "\t"
						+ query.getStatusString());

				// query.printRefEntries();
			} catch (UnreadableException e1) {
				e1.printStackTrace();
			}

		}
	}
}

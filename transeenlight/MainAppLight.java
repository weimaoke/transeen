package transeenlight;

import transeen.util.OptionLoader;

import transeen.util.TextReadWriter;
import transeen.database.*;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator; //import java.util.Vector;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

//import jade.wrapper.StaleProxyException;

public class MainAppLight {
	protected static OptionLoader optionLoader = null;
	protected static DatabaseFactory db = null;

	/**
	 * Start the program...
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		Runtime rt = Runtime.instance();
		rt.setCloseVM(true);

		optionLoader = new OptionLoader(null);
		/**
		 * Set default values for host and port and read arguments from command
		 * line.
		 */
		String node = "0";
		String mainHost = "localhost";
		String mainPort = "1099";
		String localHost = "localhost";
		String localPort = "1100";
		String containerPrefix = "LAIR";
		int rewireStart = 0;
		int rewireAddRandomNeighbors = 20;

		mainHost = optionLoader.getOptionString("mainHost");
		mainPort = optionLoader.getOptionString("mainPort");
		localHost = optionLoader.getOptionString("localHost");
		localPort = optionLoader.getOptionString("localPort");
		containerPrefix = optionLoader.getOptionString("containerPrefix");
		rewireStart = optionLoader.getOptionInt("rewireStart");
		rewireAddRandomNeighbors = optionLoader
				.getOptionInt("rewireAddRandomNeighbors");

		// NODE
		if (args.length > 0) {
			node = args[0];
			node = node.trim();
			if (node.equals("")) {
				node = "0";
			}
		}

		// LOCALHOST
		if (args.length > 1) {
			localHost = args[1];
		}

		// LOCALHOST
		if (args.length > 2) {
			localPort = args[2];
		}

		// Start the experiment or not
		int startExp = optionLoader.getOptionInt("startExperiment");
		if (args.length > 3) {
			startExp = new Integer(args[3]).intValue();
		} else {
			startExp = 0;
		}

		/**
		 * Create a new main container
		 */
		Profile p = new ProfileImpl();
		p.setParameter(Profile.PLATFORM_ID, "TRANSEEN");
		p.setParameter(Profile.MAIN_HOST, mainHost);
		p.setParameter(Profile.MAIN_PORT, mainPort);
		// p.setParameter(Profile.MASTER_NODE_NAME, containerPrefix);
		p.setParameter(Profile.ACCEPT_FOREIGN_AGENTS, "true");
		// jade_core_messaging_MessageManager_maxqueuesize
		ContainerController ac = null;
		/**
		 * Parameter
		 */
		if (node.equals("0")) {
			/**
			 * node 0 deletes the DONE file, which keeps track of the status of
			 * experiments
			 */
			File doneFile = new File(optionLoader.getOverallDoneFileName());
			if (doneFile.exists()) {
				try {
					if (doneFile.delete()) {
						System.out.println("DEBUG\t"
								+ optionLoader.getOverallDoneFileName()
								+ "\thas been deleted.");
					} else {
						System.out.println("DEBUG\t"
								+ optionLoader.getOverallDoneFileName()
								+ "\thas NOT been deleted.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			p.setParameter("jade_core_messaging_MessageManager_maxqueuesize",
					optionLoader.getOptionString("messageManagerMaxSize"));
			p.setParameter(Profile.CONTAINER_NAME, containerPrefix);
			p.setParameter(Profile.LOCAL_HOST, mainHost);
			p.setParameter(Profile.LOCAL_PORT, mainPort);
			p.setParameter(Profile.MAIN, "true");
			ac = rt.createMainContainer(p);
			System.out.println("Transeen: Starting an agent."); 
			/**
			 * start agents
			 */
			startAgents("1", ac);
			//int numAgents = optionLoader.getOptionInt("numAgents");
		} else {
			p.setParameter("jade_core_messaging_MessageManager_maxqueuesize",
					optionLoader.getOptionString("messageManagerMaxSize"));
			p.setParameter(Profile.CONTAINER_NAME, containerPrefix + node);
			p.setParameter(Profile.LOCAL_HOST, localHost);
			p.setParameter(Profile.LOCAL_PORT, localPort);
			p.setParameter(Profile.MAIN, "false");
			ac = rt.createAgentContainer(p);
			System.out.println("Transeen: Starting a query agent."); 
			startQueryAgent(ac);
		}
		System.out.println("JADE Profile:\n" + p.toString());
	}

	/**
	 * start agents from file
	 * 
	 * @return the ID/number of the last agent created
	 */
	private static int startAgents(String node, ContainerController ac) {
		int endSID = 1;
		int minD = 1;
		int maxD = 100;
		try {
			/**
			 * basic information
			 */
			String tbDomains = optionLoader.getOptionString("tbDomains");
			db = new DatabaseFactory();

			// Get summary information of all domains
			db.connect();
			PreparedStatement st = db
					.prepareStatement("select min(degree) as mind,max(degree) as maxd,max(sid) as endSID from " + tbDomains);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				minD = rs.getInt("mind");
				maxD = rs.getInt("maxd");
				endSID = rs.getInt("endSID");
			}
			db.disconnect();
			
			/**
			 * start agent 
			 */
			AgentController agent = null;
			String agentName = "ALLAGENT";
			int agentDegree = 1;
			int agentNumPages = 1;
			int agentSID = 1;
			
			// System.out.println(agentName + "\t" + agentDegree + "\t" +
			// agentNumPages);
			Integer[] paras = new Integer[5];
			paras[0] = new Integer(agentSID);
			paras[1] = new Integer(agentDegree);
			paras[2] = new Integer(agentNumPages);
			paras[3] = new Integer(minD);
			paras[4] = new Integer(maxD);
			if (agentName != null && !agentName.equalsIgnoreCase("")) {
				agent = ac.createNewAgent(agentName,
						"transeenlight.TSAgentLight", paras);
				agent.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return endSID;
	}
	
	/**
	 * start the query agent
	 * @param ac the container controller
	 */
	private static void startQueryAgent(ContainerController ac){
		/**
		 * Create new agents
		 */
		try {
			// Load query agent to start experiment
			AgentController agent = null;
			if (optionLoader.getOptionInt("startExperiment") > 0) {
				// Wait for a while for all agents to initialize
				int t = optionLoader.getOptionInt("queryAgentWait");
				//Thread.sleep(t * 1000);
				agent = ac.createNewAgent("ALLQUERY",
						"transeenlight.TSQueryAgentLight", null);
				agent.start();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
